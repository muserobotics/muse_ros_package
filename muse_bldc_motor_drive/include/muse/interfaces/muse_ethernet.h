/** @file */

/*
 *
 *  muse_ethernet.h - MUSE ethernet communication definitions
 *  Pavlos Stavrou
 *  Copyright (C) 2015 MuseRobotics Incorporated - http://www.muserobotics.com
 *  04/08/2015
 *
 */
#ifndef __MUSE_ETHERNET_H__
#define __MUSE_ETHERNET_H__

#include <stdint.h>
#include <iostream>

#include "protocols/rtmcp/rtmcp_core.h"

/// @cond HIDDEN_SYMBOLS

#define ASYNC							

#define BROADCAST_PORT 					2011

#define UDP_PORT_COM					5011
#define UDP_PORT_MONITOR				5021

#define CONNECTION_TIMEOUT_HZ 			50
#define ERROR_TIMEOUT_MS      			(1000 / CONNECTION_TIMEOUT_HZ)
#define RECEIVE_TIMEOUT_US    			600

#define TIMEOUT_TOLERANCE				20

#define TIMEOUT_TOLERANCE_MONITOR		10

#define TIMEOUT_RETRIES					1

#define RECEIVE_TIMEOUT_MONITOR_US		100000

#define GLOBAL_COMMAND_CONNECT			0x0001
#define GLOBAL_COMMAND_DISCONNECT		0xFFFF
#define GLOBAL_COMMAND_EMERGENCY		0xEFFF

#define MESSAGE_LENGTH 				sizeof(muse_message_generic)

/// @endcond


#endif
