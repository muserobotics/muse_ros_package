/** *************************************************************************************
 *
 *  Authors:        Pavlos Stavrou, pavlos@muserobotics.com
 *
 *  File:           rtcp_pmsm.h
 *
 *  Description:    RTMC Communication Protocol commands specialized for PMSM based
 *                  actuator modules. These definitions are for the 24-byte payload
 *                  size variant of the RTMC Protocol.
 *
 *
 *  Copyright (C) 2015 MuseRobotics Incorporated - http://www.muserobotics.com
 *
 ***************************************************************************************/

#ifndef _RTMCP_PMSM_H_
#define _RTMCP_PMSM_H_

#ifdef __cplusplus
extern "C"
{
#endif

/* Standard Headers */
#include <stdint.h>

#include "rtmcp_core.h"

/**
 *  RTMC PMSM Message Types
 *
 */
#define RTMCP_MESSAGE_TYPE_NOP                       (0x00)
#define RTMCP_MESSAGE_TYPE_CONNECT                   (0x01)
#define RTMCP_MESSAGE_TYPE_FSM_CMD                   (0x02)
#define RTMCP_MESSAGE_TYPE_CONTROL                   (0x03)
#define RTMCP_MESSAGE_TYPE_CONFIGURE_SET             (0x04)
#define RTMCP_MESSAGE_TYPE_CONFIGURE_GET             (0x05)
#define RTMCP_MESSAGE_TYPE_CALIBRATE                 (0x06)
#define RTMCP_MESSAGE_TYPE_ACK                       (0x07)
#define RTMCP_MESSAGE_TYPE_IP_MAC                    (0x08)
#define RTMCP_MESSAGE_TYPE_NAME                      (0x09)
#define RTMCP_MESSAGE_TYPE_DEVICE_INFO               (0x0A)

/**
 *  RTMC State IDs
 *
 */
#define RTMC_FSM_STATE_INIT                         (0x00)
#define RTMC_FSM_STATE_COLD_RESET                   (0x01)
#define RTMC_FSM_STATE_WARM_RESET                   (0x02)
#define RTMC_FSM_STATE_CONFIG                       (0x03)
#define RTMC_FSM_STATE_CALIBRATE                    (0x04)
#define RTMC_FSM_STATE_STANDBY                      (0x05)
#define RTMC_FSM_STATE_MOTOR_ON                     (0x06)
#define RTMC_FSM_STATE_CONTROL_ON                   (0x07)
#define RTMC_FSM_STATE_MOTOR_ID                     (0x08)
#define RTMC_FSM_STATE_FAULT                        (0x0E)
#define RTMC_FSM_STATE_ERROR                        (0x0F)

/**
 *  RTMC State-Transition Commands
 *
 */
#define RTMC_FSM_CMD_NOP                            (0x00)
#define RTMC_FSM_CMD_RESTART                        (0x01)
#define RTMC_FSM_CMD_STARTUP                        (0x02)
#define RTMC_FSM_CMD_SHUTDOWN                       (0x03)
#define RTMC_FSM_CMD_CALIBRATE_START                (0x04)
#define RTMC_FSM_CMD_CALIBRATE_STOP                 (0x05)
#define RTMC_FSM_CMD_MOTOR_ENABLE                   (0x06)
#define RTMC_FSM_CMD_MOTOR_DISABLE                  (0x07)
#define RTMC_FSM_CMD_CONTROL_ENABLE                 (0x08)
#define RTMC_FSM_CMD_CONTROL_DISABLE                (0x09)
#define RTMC_FSM_CMD_ERROR                          (0x0A)
#define RTMC_FSM_CMD_ERRORS_CLEARED                 (0x0B)
#define RTMC_FSM_CMD_FAULT                          (0x0C)
#define RTMC_FSM_CMD_FAULT_RESET                    (0x0D)
#define RTMC_FSM_CMD_DISCONNECT_RESET               (0x0E)
#define RTMC_FSM_CMD_QUICK_ENABLE                   (0x0F)
#define RTMC_FSM_CMD_MOTOR_ID_START                 (0x10)
#define RTMC_FSM_CMD_MOTOR_ID_STOP                  (0x11)

/**
 *  RTMC PMSM Configurations
 */

#define RTMCP_CONFIG_MODE_ENCODER                    (0x01)
#define RTMCP_CONFIG_MODE_CONTROL_LIMITS             (0x02)
#define RTMCP_CONFIG_MODE_CURRENT_GAINS              (0x03)
#define RTMCP_CONFIG_MODE_SPEED_GAINS                (0x04)
#define RTMCP_CONFIG_MODE_POSITION_GAINS             (0x05)
#define RTMCP_CONFIG_MODE_MOTION_GAINS               (0x06)
#define RTMCP_CONFIG_MODE_SAVE                       (0x07)
#define RTMCP_CONFIG_MODE_MOTOR_PARAMS               (0x08)
#define RTMCP_CONFIG_MODE_MOTOR_LIMITS               (0x09)
#define RTMCP_CONFIG_MODE_RESET                      (0x0A)
#define RTMCP_CONFIG_MODE_RESTORE                    (0x0B)
#define RTMCP_CONFIG_MODE_CONTROL_OPTIONS            (0x0C)

/**
 *  RTMC PMSM Calibrations
 */
#define RTMCP_CALIBR_MODE_NOP                        (0x0000)
#define RTMCP_CALIBR_MODE_ADC_ELEC_BIAS              (0x0001)
#define RTMCP_CALIBR_MODE_ELEC_ANGLE_OFFSET          (0x0002)
#define RTMCP_CALIBR_MODE_MOTOR_ID_ELEC_HIGH_FREQ    (0x0004)
#define RTMCP_CALIBR_MODE_MOTOR_ID_ELEC_DC           (0x0008)
#define RTMCP_CALIBR_MODE_MOTOR_ID_ROTOR_FLUX        (0x0010)
#define RTMCP_CALIBR_MODE_CURRENT_CONTROL            (0x0020)
#define RTMCP_CALIBR_MODE_STATIC_FRICTION            (0x0040)
#define RTMCP_CALIBR_MODE_VISCOUS_FRICTION           (0x0080)
#define RTMCP_CALIBR_MODE_MECH_INERTIA               (0x0100)
#define RTMCP_CALIBR_MODE_ALL                        (0x8000)

/**
 *  RTMC PMSM Errors & Faults
 */
#define RTMCP_FAULT_MODE_ADC_PHASE_CURRENT_ERROR     (0x01)
#define RTMCP_FAULT_MODE_ADC_PHASE_VOLTAGE_ERROR     (0x02)
#define RTMCP_FAULT_MODE_ADC_BUS_VOLTAGE_ERROR       (0x03)
#define RTMCP_FAULT_MODE_TEMPERATURE_CRITICAL        (0x04)
#define RTMCP_FAULT_MODE_ENCODER_MALFUNCTION         (0x05)
#define RTMCP_FAULT_MODE_MOTOR_LOCKED                (0x06)

#define RTMCP_ERROR_MODE_CONNECTION_TIMEOUT          (0x01)
#define RTMCP_ERROR_MODE_OVERVOLTAGE_OVERCURRENT     (0x02)
#define RTMCP_ERROR_MODE_TEMPERATURE_WARNING         (0x03)

/**
 *  RTMC PMSM Data & Configuration Overview
 *
 */
#define RTMCP_PMSM_CONFIGS_NUM                       (14)
#define RTMCP_PMSM_CALIBS_NUM                        (16)
#define RTMCP_PMSM_DATA_STORAGE_NUM                  (RTMCP_PMSM_CONFIGS_NUM + RTMCP_PMSM_CALIBS_NUM)

/**
 *  RTMC PMSM Master Data Struct
 *
 */
typedef struct
{
    uint16_t fsm_state;
    uint16_t fsm_command;

    rtmcp_errors_faults_t rtmc_errors_faults;
    rtmcp_errors_faults_t comm_errors_faults;

    rtmcp_calibration_t calibr_mode;
    uint16_t config_mode;
    MotorControlMode_t control_mode;

    _iq24 fb_current_A;
    _iq24 fb_speed_krpm;
    _iq16 fb_position_pu_iq16;

    _iq24 sp_current_A;
    _iq24 sp_speed_krpm;
    _iq16 sp_position_pu_iq16;

    uint16_t bus_voltage;
    uint16_t temperature_mcu;
    uint16_t temperature_pcb;

    _iq16 conf_position_zero_offset_pu_iq16;
    _iq16 conf_gear_ratio;

    _iq24 conf_current_max_A;     // current limit
    _iq24 conf_speed_max_krpm;      // speed limit
    _iq24 conf_accel_max_krpmps;    // acceleration limit
    _iq16 conf_position_max_pu_iq16;  // Upper position limit
    _iq16 conf_position_min_pu_iq16;  // Lower position limit

    _iq24 motor_current_max_A;           // motor current limit
    _iq24 motor_speed_max_krpm;          // motor speed limit
    _iq24 motor_accel_max_krpmps;        // motor acceleration limit

    uint16_t numPolePairs;
    float Rs;              
    float Ls;              
    float ratedFlux;

    EncoderDeviceConfig_t conf_motor_encoder_type;
    EncoderDeviceConfig_t conf_output_encoder_type;
    uint32_t conf_motor_encoder_resolution;
    uint32_t conf_output_encoder_resolution;

    // Controller gains
    _iq24 conf_current_p_gain;
    _iq24 conf_current_i_gain;
    _iq24 conf_speed_p_gain;
    _iq24 conf_speed_i_gain;
    _iq24 conf_position_p_gain;
    _iq24 conf_position_i_gain;
    _iq24 conf_position_d_gain;
    _iq22 conf_motion_p_gain;
    _iq22 conf_motion_i_gain;
    _iq22 conf_motion_d_gain;

    _iq24 calibr_motor_hf_resistance_Ohm_iq;
    _iq24 calibr_motor_hf_inductance_H_iq;
    _iq24 calibr_motor_resistance_Ohm_iq;
    _iq24 calibr_motor_inductance_H_iq;
    _iq24 calibr_motor_flux_linkage_VpHz_iq;
    _iq24 calibr_elec_angle_offset_pu_iq;
    _iq24 calibr_adc_current_a_bias_iq;
    _iq24 calibr_adc_current_b_bias_iq;
    _iq24 calibr_adc_current_c_bias_iq;
    _iq24 calibr_adc_voltage_a_bias_iq;
    _iq24 calibr_adc_voltage_b_bias_iq;
    _iq24 calibr_adc_voltage_c_bias_iq;
    _iq24 calibr_mech_friction_static_Nm_iq;
    _iq24 calibr_mech_friction_coulomb_Nm_iq;
    _iq24 calibr_mech_friction_viscous_Nm_iq;
    _iq24 calibr_mech_inertia_moment_Nm_iq;

    uint16_t HW_version;                
    uint16_t FW_version;                
    uint16_t spi_encoder_ports;
    uint16_t abi_encoder_ports;
    uint16_t HW_options;

    _iq24 conf_sensor_max_speed_krpm;
    CommutationMode_t conf_commutation_mode;
    SpeedSourceMode_t conf_speed_source;
    PositionSourceMode_t conf_position_source;   

} rtmcp_pmsm_t;

#define RTMCP_PMSM_DATA_INIT  { \
    0,      /*  fsm_state */ \
    0,      /*  fsm_command */ \
    0,      /*  rtmc_errors_faults */ \
    0,      /*  comm_errors_faults */ \
    0,      /*  calibr_mode */ \
    0,      /*  config_mode */ \
    DiagnosticControl,  /* control_mode */ \
    FLOAT_TO_IQ24(0.0), /*  fb_current_A */ \
    FLOAT_TO_IQ24(0.0), /*  fb_speed_krpm */ \
    FLOAT_TO_IQ16(0.0), /*  fb_position_pu_iq16 */ \
    FLOAT_TO_IQ24(0.0), /*  sp_current_A */ \
    FLOAT_TO_IQ24(0.0), /*  sp_speed_krpm */ \
    FLOAT_TO_IQ24(0.0), /*  sp_position_pu_iq16 */ \
    FLOAT_TO_IQ8(0.0),  /*  bus_voltage */ \
    FLOAT_TO_IQ8(0.0),  /*  temperature_mcu */ \
    FLOAT_TO_IQ8(0.0),  /*  temperature_pcb */ \
    FLOAT_TO_IQ16(0.0), /*  conf_position_zero_offset_pu_iq16 */ \
    FLOAT_TO_IQ16(1.0), /*  conf_gear_ratio */ \
    FLOAT_TO_IQ24(0.0), /*  conf_current_max_A; */ \
    FLOAT_TO_IQ24(0.0), /*  conf_speed_max_krpm */ \
    FLOAT_TO_IQ24(MOTOR_MAX_ACCEL_KRPMPS),  /*  conf_accel_max_krpmps */ \
    MAX_IQ_POS, /*  conf_position_max_pu_iq16 */ \
    MAX_IQ_NEG, /*  conf_position_min_pu_iq16 */ \
    FLOAT_TO_IQ24(0.0), /*  motor_current_max_A */\
    FLOAT_TO_IQ24(0.0), /*  motor_speed_max_krpm */\
    FLOAT_TO_IQ24(0.0), /*  motor_accel_max_krpmps */\
    0,                  /*  numPolePairs  */\
    0.0,                /*  Rs  */\
    0.0,                /*  Ls  */\
    0.0,                /*  ratedFlux  */\
    {0, 0, Encoder_None, 0},       /*  conf_motor_encoder_type*/\
    {0, 0, Encoder_None, 0},       /*  conf_output_encoder_type*/\
    0,                  /*  conf_motor_encoder_resolution*/\
    0,                  /*  conf_output_encoder_resolution*/\
    FLOAT_TO_IQ24(0.0), /*  conf_current_p_gain */ \
    FLOAT_TO_IQ24(0.0), /*  conf_current_i_gain */ \
    FLOAT_TO_IQ24(0.0), /*  conf_velocity_p_gain */ \
    FLOAT_TO_IQ24(0.0), /*  conf_velocity_i_gain  */ \
    FLOAT_TO_IQ24(0.0), /*  conf_position_p_gain  */ \
    FLOAT_TO_IQ22(0.0), /*  conf_motion_p_gain  */ \
    FLOAT_TO_IQ22(0.0), /*  conf_motion_i_gain    */ \
    FLOAT_TO_IQ22(0.0), /*  conf_motion_d_gain    */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_motor_hf_resistance_Ohm_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_motor_hf_inductance_H_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_motor_resistance_Ohm_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_motor_inductance_H_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_motor_flux_linkage_VpHz_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_elec_angle_offset_pu_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_adc_current_a_bias_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_adc_current_b_bias_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_adc_current_c_bias_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_adc_voltage_a_bias_iq  */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_adc_voltage_b_bias_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_adc_voltage_c_bias_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_mech_friction_static_Nm_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_mech_friction_coulomb_Nm_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_mech_friction_viscous_Nm_iq   */ \
    FLOAT_TO_IQ24(0.0),      /*  calibr_mech_inertia_moment_Nm_iq   */ \
    0,                          /*  HW_version  */\
    0,                          /*  FW_version  */\
    0,                          /*  spi_encoder_ports  */\
    0,                          /*  abi_encoder_ports  */\
    0,                          /*  HW_options  */\
    FLOAT_TO_IQ24(0.0),         /*  conf_sensor_max_speed_krpm   */ \
    Commutation_Sensorless,     /*  conf_commutation_mode  */\
    Estimator_SpeedSource,      /*  conf_speed_source  */\
    PositionSource_None         /*  conf_position_source  */\
};

static inline void pmsm_get_motion_setpoint_data (volatile rtmcp_message_t *buffer,
                                                  volatile rtmcp_pmsm_t *data)
{
    data->sp_speed_krpm = buffer->speed;
    data->sp_position_pu_iq16 = buffer->position;
};

static inline void pmsm_set_motion_feedback_data (volatile rtmcp_message_t *buffer,
                                                  volatile rtmcp_pmsm_t *data)
{
    buffer->speed = data->fb_speed_krpm ;
    buffer->position = data->fb_position_pu_iq16 ;
};

#ifdef __cplusplus
}
#endif

#endif

/* EOF */
