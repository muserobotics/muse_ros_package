/** *************************************************************************************
 *
 *  Authors:        Pavlos Stavrou, pavlos@muserobotics.com
 *
 *  File:           rtmcp_core.h
 *
 *  Description:    RTMC Core Communication Protocol. This header serves as the primary
 *                  front-end for utilizing the RTMC Standard Messages.
 *
 *
 *  Copyright (C) 2015 SkyRobotics Incorporated - http://www.muserobotics.com
 *
 ***************************************************************************************/

#ifndef _RTMCP_CORE_H_
#define _RTMCP_CORE_H_

#ifdef __cplusplus
extern "C"
{
#endif

/* Standard Headers */
#include <stdint.h>

typedef int32_t _iq16;
typedef int32_t _iq22;
typedef int32_t _iq24;
typedef uint16_t _iq8;

#define TYPE_IQ16    0
#define TYPE_IQ22    1
#define TYPE_IQ24    2

#define FLOAT_TO_IQ24(value_fp)  ((int32_t)(value_fp * 16777216.0F))
#define IQ24_TO_FLOAT(value_iq)  ((double)value_iq / 16777216.0F)
#define FLOAT_TO_IQ22(value_fp)  ((int32_t)(value_fp * 4194304.0F))
#define IQ22_TO_FLOAT(value_iq)  ((double)value_iq / 4194304.0F)

#define FLOAT_TO_IQ16(value_fp)  ((int32_t)(value_fp * 65536.0))
#define IQ16_TO_FLOAT(value_iq)  ((double)value_iq / 65536.0F)
#define FLOAT_TO_IQ8(value_fp)  ((uint16_t)(value_fp * 256.0F))
#define IQ8_TO_FLOAT(value_iq)  ((double)value_iq / 256.0F)


#ifndef MOTOR_MAX_ACCEL_KRPMPS
#define MOTOR_MAX_ACCEL_KRPMPS (0.2)
#endif

#ifndef MAX_IQ_POS
#define MAX_IQ_POS         2147483647      /* MAX VALUE FOR LONG               */
#endif

#ifndef MAX_IQ_NEG
#define MAX_IQ_NEG        (-MAX_IQ_POS-1)    /* MIN VALUE FOR LONG               */
#endif


typedef enum {
    CurrentController_Disabled = 0,
    CurrentController_Default = 1,
    CurrentController_Optimised = 2,
    CurrentController_NoEST = 3,
} CurrentController_t;

typedef enum {
    Commutation_Sensorless = 0,
    Commutation_Sensored,
    Commutation_SensoredHybrid,
    Commutation_Hall,
    Commutation_HallHybrid   
} CommutationMode_t;

typedef enum {
    Estimator_SpeedSource     = 0,
    Encoder_SpeedSource     = 1,
    Hall_SpeedSource        = 2,
    EncoderHybrid_SpeedSource      = 3,
} SpeedSourceMode_t;

typedef enum {
    PositionSource_None             = 0,
    PositionSource_MotorEncoder     = 1,
    PositionSource_OutputEncoder    = 2,
} PositionSourceMode_t;

typedef enum {
    DiagnosticControl           = 0,
    FreezeControl               = 1,
    ReleaseControl              = 2,
    MotionControl               = 3,
    CurrentControl              = 4,
    TorqueControl               = 5,
    SpeedControl                = 6,
//    MultiTurnPositionControl    = 7,
    PositionControl             = 8,
    UserControl                 = 9,
    MotorIdle                   = 100
} MotorControlMode_t;

typedef enum {
    Encoder_None    = 0,
    Encoder_AS5047P = 1,
    Encoder_AS5311  = 2,
    Encoder_QEI     = 3,
} EncoderType_t;

/* Core Real-Time Message Definition */
typedef struct
{
    uint16_t message_type:8;    // 0-7
    uint16_t args:8;        // 8-15
} rtmcp_header_fields_t;

typedef union
{
    rtmcp_header_fields_t fields;
    uint16_t value;
} rtmcp_header_t;

typedef struct
{
    uint16_t fsm_state:8;   // 0-7
    uint8_t control_mode;      // 8-15
} rtmcp_control_state_fields_t;

typedef union
{
    rtmcp_control_state_fields_t fields;
    uint16_t value;
} rtmcp_control_state_t;

typedef struct
{
   uint16_t Phase_short:1;          // 0
   uint16_t Vbus_UV:1;              // 1
   uint16_t Vbus_OV:1;              // 2
   uint16_t Temp_fault:1;           // 3
   uint16_t Gate_Driver_fault:1;    // 4
   uint16_t PowerStageFault:1;      // 5
   uint16_t EncoderFault:1;         // 6
   uint16_t ConfigFault:1;          // 7
   uint16_t dummy:8;                // 8-15
} rtmcp_faults_bits_t;

typedef union
{
   rtmcp_faults_bits_t bits;
   uint16_t value;
} rtmcp_faults_t;

typedef struct
{
   uint16_t Vbus_UV:1;              // 0
   uint16_t Vbus_OV:1;              // 1
   uint16_t Temp_error:1;           // 2
   uint16_t Encoder_Calibration:1;  // 3
   uint16_t ConfigError:1;          // 4
   uint16_t dummy:11;               // 5-15
} rtmcp_errors_bits_t;

typedef union
{
   rtmcp_errors_bits_t bits;
   uint16_t value;
} rtmcp_errors_t;

typedef struct
{
   rtmcp_faults_t faults;
   rtmcp_errors_t errors;
} rtmcp_errors_faults_fields_t;

typedef union
{
   uint32_t value;
   rtmcp_errors_faults_fields_t fields;
}rtmcp_errors_faults_t;

typedef struct
{
    uint16_t adc_elec_bias:1;           // 0
    uint16_t elec_angle_offset:1;       // 1
    uint16_t motor_id_elec_high_freq:1; // 2
    uint16_t motor_id_elec_dc:1;        // 3
    uint16_t motor_id_rotor_flux:1;     // 4
    uint16_t current_control:1;         // 5
    uint16_t static_friction:1;         // 6
    uint16_t viscous_friction:1;        // 7
    uint16_t mech_inertia:1;            // 8
    uint16_t reserved:6;                // 9-14
    uint16_t all:1;                     // 15
} rtmcp_calibration_fields_t;

typedef union
{
    rtmcp_calibration_fields_t fields;
    uint16_t value;
} rtmcp_calibration_t;

typedef struct __attribute__((packed, aligned(1))){
    uint16_t direction:1;           // 0
    uint16_t port:2;                // 1-2
    EncoderType_t EncoderType:6;    // 3-8
    uint16_t reserved:7;            // 9-15
} EncoderDeviceConfig_t;

typedef struct __attribute__((packed, aligned(1))) 
{
    _iq16 position_zero_offset_pu;                // 0-3
    _iq16 gear_ratio;                             // 4-7
    EncoderDeviceConfig_t motor_encoder_type;     // 8-9
    EncoderDeviceConfig_t output_encoder_type;    // 10-11
    uint32_t motor_encoder_resolution;            // 12-15
    uint32_t output_encoder_resolution;           // 16-19
}rtmcp_encoder_config_t;

typedef struct {
    uint16_t spi_encoder_ports:2;
    uint16_t abi_encoder_ports:2;
    uint16_t reserved:12;
} encoder_support_t;

typedef struct
{
    rtmcp_header_t header;              // 0-1
    uint16_t arg;                       // 2-3
    uint16_t HW_version;                // 4-5
    uint16_t FW_version;                // 6-7
    encoder_support_t encoder_support;  // 8-9
    uint16_t HW_options;                // 10-11
    uint16_t reserved[6];               // 12-23
} rtmcp_device_info_message_t;

typedef struct 
{
    _iq24 current_max_A;
    _iq24 speed_max_krpm;
    _iq24 accel_max_krpmps;
    _iq16 position_max_pu;
    _iq16 position_min_pu;
}rtmcp_limits_config_t;

typedef struct
{
   _iq24 p_gain;                        // 0-3
   _iq24 i_gain;                        // 4-7
   uint16_t reserved[6];                // 8-19
}rtmcp_current_gains_config_t;

typedef struct
{
   _iq24     p_gain;                    // 0-3
   _iq24     i_gain;                    // 4-7
   uint16_t reserved[6];                // 8-19
}rtmcp_speed_gains_config_t;

typedef struct
{
   _iq24 p_gain;                            // 0-3
   _iq24 i_gain;                            // 4-7
   _iq24 d_gain;                            // 8-11
   uint16_t reserved[4];                    // 12-19

}rtmcp_position_gains_config_t;

typedef struct 
{
    _iq22 p_gain;
    _iq22 i_gain;
    _iq22 d_gain;
    uint16_t reserved[4];
}rtmcp_motion_gains_config_t;

typedef struct
{
    uint16_t encoder_config:1;  // 0
    uint16_t control_limits:1;    // 1
    uint16_t current_gains:1;   // 2
    uint16_t speed_gains:1; // 3
    uint16_t position_gains:1;  // 4
    uint16_t motion_gains:1;    // 5
    uint16_t motor_params:1;    // 6
    uint16_t motor_limits:1;    // 7
    uint16_t control_options:1;    // 8
    uint32_t reserved:23;   // 9-31
} config_contents_fields_t;

typedef union
{
    uint32_t value;
    config_contents_fields_t fields;
} config_contents_t;

typedef struct 
{
    config_contents_t config_contents;
    uint16_t reserved[8];
}rtmcp_save_config_t;

typedef struct
{  
   float Rs;               // 0-3
   float Ls;               // 4-7
   float ratedFlux;        // 8-11
   uint16_t numPolePairs;  // 12-13
   uint16_t filler[3];     // 14-19
}rtmcp_motor_params_config_t;

typedef struct
{
   _iq24 current_max_A;        // 0-3
   _iq24 speed_max_krpm;       // 4-7
   _iq24 accel_max_krpmps;     // 8-11
   uint32_t filler[2];         // 12-19
}rtmcp_motor_limits_config_t;

typedef struct __attribute__((packed, aligned(1)))
{
    _iq24 sensor_max_speed_krpm;            // 0-3
    CommutationMode_t commutation_mode:16;     // 4-5
    SpeedSourceMode_t speed_source:16;         // 6-7
    PositionSourceMode_t position_source:16;   // 8-9
    uint16_t reserved[5];                   // 10-19
} rtmcp_control_options_t;

typedef union
{
    rtmcp_encoder_config_t encoder_config;
    rtmcp_limits_config_t limits_config;
    rtmcp_current_gains_config_t current_gains_config;
    rtmcp_speed_gains_config_t speed_gains_config;
    rtmcp_position_gains_config_t position_gains_config;
    rtmcp_motion_gains_config_t motion_gains_config;
    rtmcp_save_config_t save_configuration;
    rtmcp_motor_params_config_t motor_params_config;
    rtmcp_motor_limits_config_t motor_limits_config;
    rtmcp_control_options_t control_options_config;
}rtmcp_config_values_t;

typedef struct
{
    rtmcp_header_t header;                  // 0-1
    rtmcp_control_state_t control_state;    // 2-3
    rtmcp_errors_faults_t errors_faults;    // 4-7
    _iq16 position;                         // 8-11
    _iq24 speed;                            // 12-15
    _iq24 current;                          // 16-19
    uint16_t VBus;                          // 20-21
    uint16_t temperature;                   // 22-23
} rtmcp_message_t;

typedef struct
{
    rtmcp_header_t header;                  // 0-1
    rtmcp_calibration_t calibration_tasks;  // 2-3
    _iq24 filler[5];                        // 4-23
} rtmcp_calibration_message_t;

typedef struct
{
    rtmcp_header_t header;                  // 0-1
    uint16_t filler;                        // 2-3
    rtmcp_config_values_t config_values;    // 4-23
} rtmcp_configuration_message_t;

typedef struct
{
    rtmcp_header_t header;                  // 0-1
    uint8_t MAC[6];                         // 2-5
    uint8_t IP[4];                          // 6-9
    uint8_t SUBNETMASK[2];                   // 10-11
    uint8_t filler[10];                     // 12-23
} rtmcp_ipmac_message_t;

typedef struct
{
    rtmcp_header_t header;                  // 0-1
    uint8_t name[16];                       // 2-17
    uint8_t filler[6];                      // 18-23
} rtmcp_name_message_t;



#ifdef __cplusplus
}
#endif

#endif

/* EOF */
