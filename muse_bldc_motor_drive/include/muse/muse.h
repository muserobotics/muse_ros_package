/*
 *
 *  muse.h - Muse system library - API
 *  Pavlos Stavrou
 *  Copyright (C) 2015 SkyRobotics Incorporated - http://www.skyrobotics.com
 *  04/08/2015
 *
*/

#ifndef __MUSE_H__
#define __MUSE_H__

#include "arpa/inet.h"
#include "netinet/in.h"
#include "stdio.h"
#include "sys/types.h"
#include "sys/socket.h"
#include "unistd.h"
#include "string.h"
#include "stdlib.h"
#include "signal.h"
#include "unistd.h"
#include "fcntl.h"
#include <stdint.h>
#include <pthread.h>
#include <string>
#include <queue>
#include <semaphore.h>

#include <iostream>
#include <map>

#define MUSE_API_VERSION	2.0

/// @cond HIDDEN_SYMBOLS

#include "interfaces/muse_ethernet.h"
#include "protocols/rtmcp/rtmcp_core.h"
#include "protocols/rtmcp/rtmcp_pmsm.h"

/// @endcond

namespace Muse
{
	/**
	 * \struct muse_message_brushless
	 * \brief The main message structure to the module contains a Real_Time Motion Control Payload
	 */
	typedef struct {
		rtmcp_message_t pmsm_payload;
	} muse_message_brushless;

	/**
	 * \struct muse_message_generic
	 * \brief A generic message struct that help identify only the message header (status/command)
	 */
	typedef struct {
		uint16_t status_command;
		uint16_t fillers[11];
	} muse_message_generic;

	/**
	 * \enum MUSE_MODULE_STATUS
	 * \brief Module Status Codes
	 */
	typedef enum {
	    MUSE_STATUS_EMPTY = '0', ///< No module currently assigned
	    MUSE_STATUS_IDLE = 'I', ///< Module available for connection
	    MUSE_STATUS_CONNECTED = 'C', ///< Module is connected to a client
	    MUSE_STATUS_DISCONNECTED = 'D', ///< Module has been disconnected from client
	    MUSE_STATUS_ERROR = 'E', ///< Module is in error state
	    MUSE_STATUS_TIMEOUT = 'T' ///< Communication timeout by the module
	} MUSE_MODULE_STATUS;

	/**
	 * \enum MUSE_COMMAND_STATUS
	 * \brief Module Status Codes
	 */
	typedef enum {
	    COMMAND_STATUS_VALUEOUTOFRANGE = -1, ///< Command parameters are out of range
	    COMMAND_STATUS_ERROR, ///< The command was not succefully sent
	    COMMAND_STATUS_OK ///< Command sent
	} MUSE_COMMAND_STATUS;

	/**
	 * \struct muse_module_info
	 * \brief Struct to store information for online modules
	 */
	typedef struct {
	    std::string module_name; ///< String to represent the assigned name of the module
	    std::string module_ip; ///< The module's IP address
	    MUSE_MODULE_STATUS module_status; ///< Enum of the module status
	    uint8_t MAC[6]; ///< The assigned MAC of the Ethernet PHY of the module
	    uint8_t NETMASK; ///< The number of 1s tha comprise the netmask
	    uint8_t timeout_counts; ///< Helper variable that holds communication timeouts for the module in order to remove it from the active module list
	} muse_module_info;

	/**
	 * \enum MUSE_HARDWARE_TYPE
	 * \brief Module Status Codes
	 */
	typedef enum {
		BOARD_BRUSHLESS_MUSE_GENERIC = 0, ///< 
	    BOARD_BRUSHLESS_MUSE_V1 = 6, ///< 
	    BOARD_BRUSHLESS_MUSE_FHS_BASED, ///<
	    BOARD_BRUSHLESS_MUSE_FHS, ///< 
	    BOARD_BRUSHLESS_ANIMAX_SIZE3_V1 = 10, ///<
	    BOARD_BRUSHLESS_ANIMAX_SIZE3_V2 = 12, ///<
	    BOARD_BRUSHLESS_ANIMAX_SIZE2_V1, ///<
	    BOARD_BRUSHLESS_MUSE_FHS_BASED_HIGH_CURRENT, ///<
	    BOARD_BRUSHLESS_MUSE_FHS_BASED_V2, ///<
	    BOARD_BRUSHLESS_MUSE_FHS_BASED_V2_HIGH_CURRENT ///<
	} MUSE_HARDWARE_TYPE;

	/**
	 * \struct muse_brushless_errors
	 * \brief Error codes for the brushless module. Errors can occure but do not prevent motor operation
	 */
	typedef struct {
		uint32_t BRUSHLESS_ERROR_MODE_UNDERVOLTAGE:1; ///< The module cannot supply the configured voltage limit
		uint32_t BRUSHLESS_ERROR_MODE_OVERVOLTAGE:1; ///< The module is close to exceeding the configured voltage limit
		uint32_t BRUSHLESS_ERROR_MODE_TEMPERATURE_WARNING:1; ///< The module's temprature is near the upper limit
		uint32_t BRUSHLESS_ERROR_MODE_ENCODER_CALIBRATION:1; ///< The module's mechanical encoder needs to be calibrated
		uint32_t BRUSHLESS_ERROR_MODE_CONFIGURATION:1; ///< The module's configuration is invalid
	} muse_brushless_errors;

	/**
	 * \union muse_brushless_error
	 * \brief Error codes for the brushless module. Ability to check each error seperately or get the error code value
	 */
	typedef union {
		muse_brushless_errors error_fields;
		uint32_t value;
	} muse_brushless_error;

	/**
	 * \struct muse_brushless_faults
	 * \brief Fault codes for the brushless module. When a fault occurs the motor is suspended to prevent damage.
	 */
	typedef struct {
		uint32_t BRUSHLESS_FAULT_MODE_OVERCURRENT:1; ///< Critical DC bus overcurrent
		uint32_t BRUSHLESS_FAULT_MODE_UNDERVOLTAGE:1; ///< Critical DC bus undervoltage
		uint32_t BRUSHLESS_FAULT_MODE_OVERVOLTAGE:1; ///< Critical DC bus overvoltage
		uint32_t BRUSHLESS_FAULT_MODE_TEMPERATURE_CRITICAL:1; ///< Module has reached critical temperature 
		uint32_t BRUSHLESS_FAULT_MODE_GATE_DRIVER_ERROR:1; ///< Internal gate driver error
		uint32_t BRUSHLESS_FAULT_MODE_POWER_STAGE_FAULT:1; ///< Generic power stage fault
		uint32_t BRUSHLESS_FAULT_MODE_ENCODER_FAULT:1; ///< Encoder malfunction or non-calibrated encoder
		uint32_t BRUSHLESS_FAULT_MODE_CONFIGURATION:1; ///< Critical configuration fault that renders the drive inoperable
	} muse_brushless_faults;

	/**
	 * \union muse_brushless_fault
	 * \brief Fault codes for the brushless module. Ability to check each fault seperately or get the fault code value
	 */
	typedef union {
		muse_brushless_faults fault_fields;
		uint32_t value;
	} muse_brushless_fault;

	/**
	 * \enum muse_brushless_control_mode
	 * \brief The various supported control modes of the MUSE platform
	 */
	typedef enum {
		BRUSHLESS_CONTROL_MODE_DIAGNOSTIC = 0, ///< Diagnostic control mode used to retrieve diagnostic data (temperature, voltage, etc.)
		BRUSHLESS_CONTROL_MODE_FREEZE, ///< This control modes freezes the motor in position. Virtual break.
		BRUSHLESS_CONTROL_MODE_RELEASE, ///< In Release control mode the controller allows free movement of the motor
		BRUSHLESS_CONTROL_MODE_MOTION, ///< To be supported in the future, moves the motor to the specified position using the input speed
		BRUSHLESS_CONTROL_MODE_CURRENT, ///< In this mode we can issue current commands directly
		BRUSHLESS_CONTROL_MODE_SPEED = 6, ///< Speed control mode
		BRUSHLESS_CONTROL_MODE_POSITION = 8, ///< Position control mode
		BRUSHLESS_CONTROL_MODE_USER, ///< Reserved for future purposes, in case the user wishes to add his own controller
		BRUSHLESS_CONTROL_MODE_IDLE = 100 ///< Idle mode of the control, does not do anything

	} muse_brushless_control_mode;

	/**
	 * \enum muse_brushless_commutation_mode
	 * \brief The various supported commutation modes of the MUSE platform
	 */
	typedef enum {
		BRUSHLESS_COMMUTATION_MODE_SENSORLESS = 0, ///< Sensorless commutation uses the estimator for electrical angle calculation.
		BRUSHLESS_COMMUTATION_MODE_SENSORED, ///< Sensored commutation uses the motor encoder for electrical angle calculation.
		BRUSHLESS_COMMUTATION_MODE_SENSOREDHYBRID, ///< Motor electrical angle is calculated using the motor encoder at low speed and the estimator at higher speed.
		BRUSHLESS_COMMUTATION_MODE_HALL, ///< Hall sensors are used for electrical angle calculation
		BRUSHLESS_COMMUTATION_MODE_HALLHYBRID ///< Motor electrical angle is calculated using the hall sensors at low speed and the estimator at higher speed.

	} muse_brushless_commutation_mode;

	/**
	 * \enum muse_brushless_speed_source
	 * \brief The various supported sources of speed calculation of the MUSE platform
	 */
	typedef enum {
		BRUSHLESS_SPEED_SOURCE_ESTIMATOR = 0, ///< The speed value is estimated by current and voltage measurements.
		BRUSHLESS_SPEED_SOURCE_ENCODER, ///< The speed value is calculated using the motor encoder.
		BRUSHLESS_SPEED_SOURCE_HALL, ///< The speed value is calculated using Hall sensors
		BRUSHLESS_SPEED_SOURCE_ENCODERHYBRID ///< The speed value is calculated using the motor encoder for low speed and the estimator for higher speed.

	} muse_brushless_speed_source;

	/**
	 * \enum muse_brushless_position_source
	 * \brief The various supported sources of position calculation of the MUSE platform
	 */
	typedef enum {
		BRUSHLESS_POSITION_SOURCE_NONE = 0, ///< No position source. Position control will be inactive.
		BRUSHLESS_POSITION_SOURCE_MOTOR_ENCODER, ///< The position source is the motor encoder.
		BRUSHLESS_POSITION_SOURCE_OUTPUT_ENCODER ///< The position source is the output encoder.

	} muse_brushless_position_source;

	/**
	 * \enum muse_brushless_fsm_state
	 * \brief The states that the internal state machine can navigate through
	 */
	typedef enum {
		BRUSHLESS_FSM_STATE_INIT = 0,  ///< State of initialization. The primary state upon startup.
		BRUSHLESS_FSM_STATE_COLD_RESET, ///<  State after hardware reset
		BRUSHLESS_FSM_STATE_WARM_RESET, ///< State after software reset
		BRUSHLESS_FSM_STATE_CONFIG, ///< In this state the module allows for parameter configuration and calibration
		BRUSHLESS_FSM_STATE_CALIBRATE, ///< State in which the module performs automatic encoder calibration
		BRUSHLESS_FSM_STATE_STANDBY, ///< State in chich the module is ready to enable the motor and commence controller execution
		BRUSHLESS_FSM_STATE_MOTOR_ON, ///< Motor enabled
		BRUSHLESS_FSM_STATE_CONTROL_ON, ///< Controller engaged
		BRUSHLESS_FSM_STATE_MOTOR_ID, ///< Ongoing Motor Identification
		BRUSHLESS_FSM_STATE_FAULT = 14, ///< State to go when a hard fault is detected
		BRUSHLESS_FSM_STATE_ERROR ///< State to go when an error is detected. Can change state after ClearError command has been issued

	} muse_brushless_fsm_state;

	/**
	 * \enum muse_brushless_encoder
	 * \brief Supported motor and output encoder types for the muse brushless module. 
	 */
	typedef enum {
		BRUSHLESS_ENCODER_NONE = 0, ///< No encoder available
		BRUSHLESS_ENCODER_AS5047P, ///< AMS AS5047P Ic
	    BRUSHLESS_ENCODER_AS5311, ///< AMS AS5311 Ic
	    BRUSHLESS_ENCODER_QEI ///< Typical QEI - ABI interface
	} muse_brushless_encoder;

	/**
	 * \class muse_msg_stack
	 * \brief Helper class for message queing
	 */
	class muse_msg_stack 
	{
	public:

		/** 
         * \brief The base constructor
         *
         * Creates an object with an empty message stack.
         */
		muse_msg_stack();
		/** 
         * \brief The base destructor
         *
         * Destroys the object.
         */
		~muse_msg_stack();

		/** 
         * \brief Push a message to the queue
         *
         * Attempt to push a message to the stack.
         * @return True or False if the message has been successfully queued or not.
         */
		bool push_element(muse_message_generic msg_brushless);

		/** 
         * \brief Pop a message from the queue
         *
         * Attempt to pop a message from the stack.
         * @return The muse module message queued or an empty message if the stack is empty
         */
		muse_message_generic pop_element();

	private:
		long int time_prev_ms;
		sem_t stack_semaphore;
		std::queue<muse_message_generic> msg_stack;
	};

	/**
	 * \class muse_module
	 * \brief The base class for all muse modules
	 */
	class muse_module
	{
		private :
			bool moduleConnected;
			void * comFunc(void);
			static void * com_helper(void *context) { return ((muse_module *)context)->comFunc(); };
			bool bRun;

		protected :
			/** 
	         * \brief muse_module_info variable to store module information
	         *
	         * @see muse_module_info
	         */
			muse_module_info modInfo;

			/** 
	         * \brief Boolean variable that manipulates communication function to only receive feedback from a muse module.
	         */
			bool monitorOnly;

			/** 
	         * \brief muse_message_generic variable
	         *
	         * Used to store the incoming message
	         * @see muse_message_generic
	         */
			muse_message_generic MsgIn;

			/** 
	         * \brief muse_message_generic variable
	         *
	         * Used to store the outgoing message
	         * @see muse_message_generic
	         */
			muse_message_generic MsgOut;

			/** 
	         * \brief muse_msg_stack variable
	         *
	         * Used to queue outgoing messages
	         * @see muse_msg_stack
	         */
			muse_msg_stack msg_stack_o;

			/** 
	         * \brief A pure virtual member.
	         *
	         * Needs to implemented for every derived module type to properly parse messages.
	         */
			virtual void parseMessage() = 0;

			/** 
	         * \brief A pure virtual member
	         *
	         * Needs to implemented for every derived module type to properly form messages
	         */
			virtual void formMessage() = 0;

		public :
			/** 
	         * \brief The base constructor
	         *
	         * Creates an object with no information stored.
	         */
			muse_module();

			/** 
	         * \brief The constructor to be used
	         *
	         * Creates an object using information stored in the muse_module_info structure.
	         * @param _mInfo The struct containing the module information (IP,MAC, etc.)
	         */
			muse_module(muse_module_info _mInfo);

			/** 
	         * \brief The base destructor
	         *
	         * Destroys the object.
	         */
			~muse_module();

			/** 
	         * \brief Connect to the module via UDP
	         *
	         * Commences the udp transaction required to communicate with the module. IP used is stored in the muse_module_info class object.
	         */
			void Connect();

			/** 
	         * \brief Disconnect from the module 
	         *
	         * Gracefully terminate the udp communication.
	         */
			void Disconnect();

			/** 
	         * \brief A virtual member
	         *
	         * Shows basic module information on standard output.
	         */
			virtual void ShowInfo();

			/** 
	         * \brief Get the module information
	         *
	         * Retrieve the module information stored in the muse_module_info class object.
	         * @return The muse_module_info struct containing all information
	         * @see muse_module_info
	         */
			muse_module_info GetInfo();

			/** 
	         * \brief Get the status of the communication thread
	         *
	         * The UDP communication with the module runs on a child thread and this function lets us know whether the thread is running or not.
	         * @return True or False if the communication thread is running or not.
	         */
			bool IsRunning();

			/** 
	         * \brief Get the status of the connection with the module
	         *
	         * Lets us know if the connection to the module is active.
	         * @return True or False if we are connected to the module or not.
	         */
			bool IsConnected();

			/** 
	         * \brief Get the status of the module
	         *
	         * Retrieve the module status as define in the MUSE_MODULE_STATUS enum.
	         * @return The MUSE_MODULE_STATUS enum store in the muse_module_info object of the class.
	         * @see MUSE_MODULE_STATUS
	         */
			MUSE_MODULE_STATUS GetStatus(void);

			/** 
	         * \brief Convert a the MAC byte array to a string
	         *
	         * Helper function to convert the MAC byte array to its string representation for printing purposes.
	         */
			std::string MAC2STR(uint8_t MAC[6]);
	};

	/* \class muse_module_monitor
	 * \brief The monitor module class.
	 *
	 * The muse_module_monitor class extends the base muse_module class and implements all required functionality to get operating parameter feedback from the muse brushless motor drive.
	 */
	class muse_module_monitor : public muse_module
	{
		public :

			/** 
	         * \brief The base constructor
	         *
	         * Creates an object with no information stored.
	         */
			muse_module_monitor();

			/** 
	         * \brief The constructor to be used
	         *
	         * Creates an object using information stored in the muse_module_info structure.
	         * @param _mInfo The struct containing the module information (IP,MAC, etc.)
	         */
			muse_module_monitor(muse_module_info _mInfo);

			/** 
	         * \brief Extension to the virtual member of the muse_class
	         *
	         * Shows basic module information on standard output along with operating information.
	         *
	         * Extended information include controller feedback, error and fault and controller state.
	         */
			void ShowInfo();

			/** 
	         * \brief Get the internal state of the controller
	         *
	         * Retrieve the current internal state of the controller.
	         * @return The muse_brushless_fsm_state enum
	         * @see muse_brushless_fsm_state
	         */
			muse_brushless_fsm_state getFsmState();

			/** 
	         * \brief Get the last known error of the controller
	         *
	         * Retrieve the last known error reported by the controller.
	         * @return The muse_brushless_error enum
	         * @see muse_brushless_error
	         */
			muse_brushless_error getError();

			/** 
	         * \brief Get the last known fault of the controller
	         *
	         * Retrieve the last known fault reported by the controller.
	         * @return The muse_brushless_fault enum
	         * @see muse_brushless_fault
	         */
			muse_brushless_fault getFault();

			/** 
	         * \brief Get current control mode engaged
	         *
	         * Retrieve the type of control running on the muse module.
	         * @return The muse_brushless_control_mode enum
	         * @see muse_brushless_control_mode
	         */
			muse_brushless_control_mode getControlMode();

			/** 
	         * \brief Get the error status of the module
	         *
	         * Lets us know if the module has reported an error.
	         * @return True or False if there is an error or not.
	         */
			bool hasServoError();

			/** 
	         * \brief Get the fault status of the module
	         *
	         * Lets us know if the module has reported a fault.
	         * @return True or False if there is a fault or not.
	         */
			bool hasServoFault();

			/** 
	         * \brief Get feedback on the motor status (enabled/disabled)
	         *
	         * Lets us know if the module has enabled the motor.
	         * @return True or False if the motor is enabled or not.
	         */
			bool isServoEnabled();

			/** 
			 * \brief Get the current motor speed feedback.
			 *
			 * Retrieve the current motor speed calculated by the speed controller.
			 * @return The motor speed feedback in KRPM
			 */
			double getSpeed();
			
			/** 
	         * \brief Get the current motor current feedback.
	         *
	         * Retrieve the current motor current calculated by the current controller.
	         * @return The motor current feedback in Ampere
	         */
			double getCurrent();
			
			/** 
	         * \brief Get the current motor position feedback.
	         *
	         * Retrieve the current motor position calculated by the position controller.
	         * @return The motor position feedback in rad
	         */
			double getPositionRad();
			
			/** 
	         * \brief Get the current motor position feedback.
	         *
	         * Retrieve the current motor position calculated by the position controller.
	         * @return The motor position feedback in PU (normalized value per rotation)
	         */
			double getPosition();
			
			/** 
	         * \brief Get the power supply bus voltage.
	         *
	         * Retrieve the bus voltage provided as input to the module by the power supply.
	         * @return The bus voltage in Volts
	         */
			double getBusVoltage();

			/** 
	         * \brief Get the temperature of the pcb.
	         *
	         * Retrieve the temperature of the pcb provided by the onboard ambient temperature sensor.
	         * @return The pcb temperature in Celsius
	         */
			double getPcbTemperature();

			/** 
	         * \brief Get the temperature of the module microcontroller.
	         *
	         * Retrieve the temperature of the microcontroller provided by the IC.
	         * @return The microcontroller temperature in Celsius
	         */
			double getMcuTemperature();
			
		private :

			void parseMessage();

			void formMessage();

			bool b_enabled;

			bool b_error;

			bool b_fault;

			rtmcp_pmsm_t o_pmsm_info;

			muse_brushless_fsm_state o_fsm_state;

			muse_brushless_control_mode o_control_mode;

			muse_brushless_error e_brushless_error;

			muse_brushless_fault e_brushless_fault;

			muse_message_brushless o_brushless_message_in;

			muse_message_brushless o_brushless_message_null;

			muse_message_generic msg_helper;
	};

	

	/**
	 * \class muse_module_brushless
	 * \brief The brushless module class.
	 *
	 * The muse_module_brushless class extends the base muse_module class and implements all required functionality to control and get feedback from the muse brushless motor drive.
	 */
	class muse_module_brushless : public muse_module
	{
		public :

			/** 
	         * \brief The base constructor
	         *
	         * Creates an object with no information stored.
	         */
			muse_module_brushless();

			/** 
	         * \brief The constructor to be used
	         *
	         * Creates an object using information stored in the muse_module_info structure.
	         * @param _mInfo The struct containing the module information (IP,MAC, etc.)
	         */
			muse_module_brushless(muse_module_info _mInfo);

			/** 
	         * \brief Extension to the virtual member of the muse_class
	         *
	         * Shows basic module information on standard output along with operating information.
	         *
	         * Extended information include controller setpoints adn feedback, error and fault and controller state.
	         */
			void ShowInfo();

			/** 
	         * \brief Prints the current configuration on standard output
	         *
	         * Outputs all configuration parameters for the suppoerted controllers to the standard output.
	         *
	         * Configuration parameters include controller gains and drive limits.
	         */
			void ShowConfig();

			/** 
	         * \brief Get the internal state of the controller
	         *
	         * Retrieve the current internal state of the controller.
	         * @return The muse_brushless_fsm_state enum
	         * @see muse_brushless_fsm_state
	         */
			muse_brushless_fsm_state getFsmState();

			/** 
	         * \brief Get the last known error of the controller
	         *
	         * Retrieve the last known error reported by the controller.
	         * @return The muse_brushless_error enum
	         * @see muse_brushless_error
	         */
			muse_brushless_error getError();

			/** 
	         * \brief Get the last known fault of the controller
	         *
	         * Retrieve the last known fault reported by the controller.
	         * @return The muse_brushless_fault enum
	         * @see muse_brushless_fault
	         */
			muse_brushless_fault getFault();

			/** 
	         * \brief Get current control mode engaged
	         *
	         * Retrieve the type of control running on the muse module.
	         * @return The muse_brushless_control_mode enum
	         * @see muse_brushless_control_mode
	         */
			muse_brushless_control_mode getControlMode();

			/** 
	         * \brief Get the error status of the module
	         *
	         * Lets us know if the module has reported an error.
	         * @return True or False if there is an error or not.
	         */
			bool hasServoError();

			/** 
	         * \brief Get the fault status of the module
	         *
	         * Lets us know if the module has reported a fault.
	         * @return True or False if there is a fault or not.
	         */
			bool hasServoFault();

			/** 
	         * \brief Get feedback on the motor status (enabled/disabled)
	         *
	         * Lets us know if the module has enabled the motor.
	         * @return True or False if the motor is enabled or not.
	         */
			bool isServoEnabled();

			/** 
	         * \brief Starts up the module
	         *
	         * Command the module to start up. Upon success the internal state changes to StandBy.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS StartUp();

			/** 
	         * \brief Shut down the module.
	         *
	         * Command the module to shut down. Upon success the internal state changes to Config.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS ShutDown();

			/** 
	         * \brief Commence motor identification.
	         *
	         * Command the module to start the motor identification process.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS startMotorId();

			/** 
	         * \brief Enable the motor and controller independent of the current state.
	         *
	         * Command the module to enable the motor and controller. Upon success the internal state changes to Control On.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS quickEnable();

			/** 
	         * \brief Enable the motor.
	         *
	         * Command the module to enable the motor. Upon success the internal state changes to Motor On.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS enableServo();

			/** 
	         * \brief Disable the motor.
	         *
	         * Command the module to disable the motor. Upon success the internal state changes to StandBy.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS disableServo();

			/** 
	         * \brief Engage the module controller.
	         *
	         * Command the module to commence controller execution. Upon success the internal state changes to ControlOn.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS controlOn();

			/** 
	         * \brief Disengege the module controller.
	         *
	         * Command the module to stop controller execution. Upon success the internal state changes to MotorOn.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS controlOff();

			/** 
	         * \brief Clear errors reported by the module.
	         *
	         * Command the module to clear its internal error register. If the error persists the register will not clear.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS clearErrors();

			/** 
	         * \brief Clear faults reported by the module.
	         *
	         * Command the module to clear its internal fault register. If the fault persists the register will not clear.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS clearFaults();

			/** 
	         * \brief Software reset the module controller.
	         *
	         * Command the module to reset its internal state machine.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS Reset();

			/** 
	         * \brief Freeze the module in position.
	         *
	         * Command the module to hold its current position. Controller needs to be engaged for this to work.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS Freeze();

			/** 
	         * \brief Release the module in order for the motor to move freely.
	         *
	         * Command the module output zero current to the motor. Controller needs to be engaged for this to work.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS Release();

			/** 
	         * \brief Go in diagnostic.
	         *
	         * Command the module to disable the controller and go into diagnostic mode.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS Diagnostic();

			/** 
	         * \brief Set the motor speed.
	         *
	         * Puts the module in Speed control mode given the speed setpoint as input.
	         * @param _speed_krpm The motor speed setpoint in KRPM
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setSpeed(double _speed_krpm);

			/** 
	         * \brief Get the current motor speed feedback.
	         *
	         * Retrieve the current motor speed calculated by the speed controller.
	         * @return The motor speed feedback in KRPM
	         */
			double getSpeed();

			/** 
	         * \brief Get the motor speed controller setpoint.
	         *
	         * Retrieve the motor speed controller setpoint.
	         * @return The motor speed setpoint in KRPM
	         */
			double getSpeedSetpoint();

			/** 
	         * \brief Set the motor current.
	         *
	         * Puts the module in Current control mode given the current setpoint as input.
	         * @param _current_A The motor current setpoint in Ampere
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setCurrent(double _current_A);

			/** 
	         * \brief Get the current motor current feedback.
	         *
	         * Retrieve the current motor current calculated by the current controller.
	         * @return The motor current feedback in Ampere
	         */
			double getCurrent();

			/** 
	         * \brief Get the motor current controller setpoint.
	         *
	         * Retrieve the motor current controller setpoint.
	         * @return The motor current setpoint in Ampere
	         */
			double getCurrentSetpoint();

			/** 
	         * \brief Set the motor position.
	         *
	         * Puts the module in Position control mode given the position setpoint as input.
	         * @param _rad The motor position setpoint in rad
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setPositionRad(double _rad);

			/** 
	         * \brief Get the current motor position feedback.
	         *
	         * Retrieve the current motor position calculated by the position controller.
	         * @return The motor position feedback in rad
	         */
			double getPositionRad();

			/** 
	         * \brief Get the motor position controller setpoint.
	         *
	         * Retrieve the motor position controller setpoint.
	         * @return The motor position setpoint in rad
	         */
			double getPositionSetpointRad();

			/** 
	         * \brief Set the motor position.
	         *
	         * Puts the module in Position control mode given the position setpoint as input.
	         * @param _pu The motor position setpoint in PU (normalized value per rotation)
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setPosition(double _pu);

			/** 
	         * \brief Get the current motor position feedback.
	         *
	         * Retrieve the current motor position calculated by the position controller.
	         * @return The motor position feedback in PU (normalized value per rotation)
	         */
			double getPosition();

			/** 
	         * \brief Get the motor position controller setpoint.
	         *
	         * Retrieve the motor position controller setpoint.
	         * @return The motor position setpoint in PU (normalized value per rotation)
	         */
			double getPositionSetpoint();

			/** 
	         * \brief Get the power supply bus voltage.
	         *
	         * Retrieve the bus voltage provided as input to the module by the power supply.
	         * @return The bus voltage in Volts
	         */
			double getBusVoltage();

			/** 
	         * \brief Get the temperature of the pcb.
	         *
	         * Retrieve the temperature of the pcb provided by the onboard ambient temperature sensor.
	         * @return The pcb temperature in Celsius
	         */
			double getPcbTemperature();

			/** 
	         * \brief Get the temperature of the module microcontroller.
	         *
	         * Retrieve the temperature of the microcontroller provided by the IC.
	         * @return The microcontroller temperature in Celsius
	         */
			double getMcuTemperature();

			/** 
	         * \brief Request the current encoder configuration values.
	         *
	         * Command the module to return the current encoder configuration values used.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestEncoderConfig();

			/** 
	         * \brief Set the encoder configuration values.
	         *
	         * Sets the encoder configuration values given as input.
	         * @param _position_zero_offset_pu The encoder offset to the zero position in PU (normalized value per rotation)
	         * @param _gear_ratio The motor output gear ratio.
	         * @param _motor_enc The encoder on the motor side.
	         * @param _motor_enc_dir The motor direction (1 for default, -1 for inverted).
	         * @param _motor_enc_portnum The numer of the port the motor encoder is connected to (0 or 1). Depends on the board hardware.
	         * @param _motor_enc_res The resolution of the motor encoder.
	         * @param _output_enc The encoder on the output side.
	         * @param _output_enc_dir The output encoder direction (1 for default, -1 for inverted).
	         * @param _output_enc_portnum The numer of the port the motor encoder is connected to (0 or 1). Depends on the board hardware.
	         * @param _output_enc_res The resolution of the output encoder.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         * @see muse_brushless_encoder
	         */
			MUSE_COMMAND_STATUS setEncoderConfig(double _position_zero_offset_pu, double _gear_ratio, muse_brushless_encoder _motor_enc, int8_t _motor_enc_dir, uint8_t _motor_enc_portnum, uint32_t _motor_enc_res, muse_brushless_encoder _output_enc, int8_t _output_enc_dir, uint8_t _output_enc_portnum, uint32_t _output_enc_res);

			/** 
	         * \brief Get the encoder configuration values.
	         *
	         * Retrieve the encoder configuration values the module uses.
	         * @param _position_zero_offset_pu The returned encoder offset to the zero position in PU (normalized value per rotation)
	         * @param _gear_ratio The returned motor output gear ratio.
	         * @param _motor_enc The encoder on the motor side.
	         * @param _motor_enc_dir The motor direction (1 for default, -1 for inverted).
	         * @param _motor_enc_portnum The numer of the port the motor encoder is connected to (0 or 1). Depends on the board hardware.
	         * @param _motor_enc_res The resolution of the motor encoder.
	         * @param _output_enc The encoder on the output side.
	         * @param _output_enc_dir The output encoder direction (1 for default, -1 for inverted).
	         * @param _output_enc_portnum The numer of the port the motor encoder is connected to (0 or 1). Depends on the board hardware.
	         * @param _output_enc_res The resolution of the output encoder.
	         * @see muse_brushless_encoder
	         */
			void getEncoderConfig(double &_position_zero_offset_pu, double &_gear_ratio, muse_brushless_encoder &_motor_enc, int8_t &_motor_enc_dir, uint8_t &_motor_enc_portnum, uint32_t &_motor_enc_res, muse_brushless_encoder &_output_enc, int8_t &_output_enc_dir, uint8_t &_output_enc_portnum, uint32_t &_output_enc_res);

			/** 
	         * \brief Set the control limit configuration values.
	         *
	         * Sets the module's limit configuration values given as input.
	         * @param _current_max_A The maximum current output to the motor in Ampere
	         * @param _speed_max_krpm The maximum allowed speed the motor should reach in KRPM
	         * @param _accel_max_krpmps The maximum allowed acceleration the motor should reach in KRPM per second
	         * @param _position_max_pu The maximum allowed position the motor should reach in PU (normalized value per rotation)
	         * @param _position_min_pu The minimum allowed position the motor should reach in PU (normalized value per rotation)
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setLimits(double _current_max_A, double _speed_max_krpm, double _accel_max_krpmps, double _position_max_pu, double _position_min_pu);

			/** 
	         * \brief Request the current control limit configuration values.
	         *
	         * Command the module to return the current limit configuration values used.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestLimits();

			/** 
	         * \brief Get the module's control limit configuration values.
	         *
	         * Retrieve the module's limit configuration values given used.
	         * @param _current_max_A The returned maximum current output to the motor in Ampere
	         * @param _speed_max_krpm The returned maximum allowed speed the motor should reach in KRPM
	         * @param _accel_max_krpmps The returned maximum allowed acceleration the motor should reach in KRPM per second
	         * @param _position_max_pu The returned maximum allowed position the motor should reach in PU (normalized value per rotation)
	         * @param _position_min_pu The returned minimum allowed position the motor should reach in PU (normalized value per rotation)
	         */
			void getLimits(double &_current_max_A, double &_speed_max_krpm, double &_accel_max_krpmps, double &_position_max_pu, double &_position_min_pu);

			/** 
	         * \brief Set the Current controller configuration gains.
	         *
	         * Sets the Current controller configuration gains given as input.
	         * @param _p_gain The P term of the PID Current controller
	         * @param _i_gain The I term of the PID Current controller
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setCurrentGains(double _p_gain, double _i_gain);

			/** 
	         * \brief Request the current Current controller configuration gains.
	         *
	         * Command the module to return the current Current configuration gains used.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestCurrentGains();

			/** 
	         * \brief Get the Current controller configuration gains used.
	         *
	         * Retrieve the Current controller configuration gains used.
	         * @param _p_gain The returned P term of the PID Current controller
	         * @param _i_gain The returned I term of the PID Current controller
	         */
			void getCurrentGains(double &_p_gain, double &_i_gain);

			/** 
	         * \brief Set the Speed controller configuration gains.
	         *
	         * Sets the Speed controller configuration gains given as input.
	         * @param _p_gain The P term of the PID Speed controller
	         * @param _i_gain The I term of the PID Speed controller
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setSpeedGains(double _p_gain, double _i_gain);

			/** 
	         * \brief Request the current Speed controller configuration gains.
	         *
	         * Command the module to return the current Speed configuration gains used.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestSpeedGains();

			/** 
	         * \brief Get the Speed controller configuration gains used.
	         *
	         * Retrieve the Speed controller configuration gains used.
	         * @param _p_gain The returned P term of the PID Speed controller
	         * @param _i_gain The returned I term of the PID Speed controller
	         */
			void getSpeedGains(double &_p_gain, double &_i_gain);

			/** 
	         * \brief Set the Position controller configuration gains.
	         *
	         * Sets the Position controller configuration gains given as input.
	         * @param _p_gain The P term of the PID Position controller
	         * @param _d_gain The D term of the PID Position controller
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setPositionGains(double _p_gain, double _d_gain);

			/** 
	         * \brief Request the current Position controller configuration gains.
	         *
	         * Command the module to return the current Position configuration gains used.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestPositionGains();

			/** 
	         * \brief Get the Position controller configuration gains used.
	         *
	         * Retrieve the Position controller configuration gains used.
	         * @param _p_gain The returned P term of the PID Position controller
	         * @param _d_gain The returned D term of the PID Position controller
	         */
			void getPositionGains(double &_p_gain, double &_d_gain);

			/** 
	         * \brief Get the motor Limits configuration used.
	         *
	         * Retrieve the motor limits configuration used.
	         * @param current_max_A The returned maximum motor current in Ampere
	         * @param speed_max_krpm The returned maximum motor speed in KRPM
	         * @param accel_max_krpmps The returned maximum motor acceleration in KRPMPS
	         */
			void getMotorLimits(double &current_max_A, double &speed_max_krpm, double &accel_max_krpmps);

			/** 
	         * \brief Set the motor limits configuration.
	         *
	         * Sets the motor limits configuration given as input.
	         * @param current_max_A The maximum motor current in Ampere
	         * @param speed_max_krpm The maximum motor speed in KRPM
	         * @param accel_max_krpmps The maximum motor acceleration in KRPMPS
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setMotorLimits(double current_max_A, double speed_max_krpm, double accel_max_krpmps);

			/** 
	         * \brief Request the current motor limits configuration.
	         *
	         * Command the module to return the current motor limits configuration.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestMotorLimits();

			/** 
	         * \brief Get the motor parameters used.
	         *
	         * Retrieve the motor paramters used.
	         * @param Rs The returned stator resistance in Ohm
	         * @param Ls The returned stator inductance in Henry
	         * @param ratedFlux The returned motor rated flux V/Hz
	         * @param numPolePairs The returned number of motor pole pairs
	         */
			void getMotorParams(float &Rs, float &Ls, float &ratedFlux, uint16_t &numPolePairs);

			/** 
	         * \brief Set the motor parameters.
	         *
	         * Sets the motor paramters given as input.
	         * @param Rs The stator resistance in Ohm
	         * @param Ls The stator inductance in Henry
	         * @param ratedFlux The motor rated flux V/Hz
	         * @param numPolePairs The number of motor pole pairs
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setMotorParams(float Rs, float Ls, float ratedFlux, uint16_t numPolePairs);

			/** 
	         * \brief Request the current motor paramters.
	         *
	         * Command the module to return the current motor paramters.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestMotorParams();

			/** 
	         * \brief Set the options for the supported controllers.
	         *
	         * Sets the module's options for the supported controllers.
	         * @param _sensor_max_speed The maximum current output to the motor in Ampere
	         * @param _commutation_mode The module's commutation mode for phase current calculation.
	         * @param _speed_source The source for speed calculation
	         * @param _position_source The source for position calculation
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         * @see muse_brushless_commutation_mode
	         * @see muse_brushless_speed_source
	         * @see muse_brushless_position_source
	         */
			MUSE_COMMAND_STATUS setControlOptions(double _sensor_max_speed, muse_brushless_commutation_mode _commutation_mode, muse_brushless_speed_source _speed_source, muse_brushless_position_source _position_source);

			/** 
	         * \brief Request options for the supported controllers.
	         *
	         * Command the module to return options for the supported controllers.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestControlOptions();

			/** 
	         * \brief Get the module's options for the supported controllers.
	         *
	         * Retrieve the module's options for the supported controllers.
	         * @param _sensor_max_speed The maximum current output to the motor in Ampere
	         * @param _commutation_mode The module's commutation mode for phase current calculation.
	         * @param _speed_source The source for speed calculation
	         * @param _position_source The source for position calculation
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         * @see muse_brushless_commutation_mode
	         * @see muse_brushless_speed_source
	         * @see muse_brushless_position_source
	         */
			void getControlOptions(double &_sensor_max_speed, muse_brushless_commutation_mode &_commutation_mode, muse_brushless_speed_source &_speed_source, muse_brushless_position_source &_position_source);

			/** 
	         * \brief Request device diagnostic information.
	         *
	         * Command the module to return the available device information (H/W and F/W version, encoder port availability, etc.).
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS requestDeviceInfo();

			/** 
	         * \brief Get the module's device information data.
	         *
	         * Retrieve the module's device information data.
	         * @param _HW_revision The device's hardware type
	         * @param _FW_version The embedded firmware version
	         * @param _spi_encoder_ports_num The number of spi ports available for encoder interfacing
	         * @param _abi_encoder_ports_num The The number of QEI (ABI) ports available for encoder interfacing
	         * @see MUSE_HARDWARE_TYPE
	         */
			void getDeviceInfo(MUSE_HARDWARE_TYPE &_HW_revision, double &_FW_version, uint16_t &_spi_encoder_ports_num, uint16_t &_abi_encoder_ports_num);

			/** 
	         * \brief Store the current configuration parameters.
	         *
	         * Command the module to store the current configuration parameters on the onboard flash.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS saveConfig();

			/** 
	         * \brief Store the selected configuration parameters.
	         *
	         * Command the module to store the selected configuration parameters on the onboard flash.
	         * @param encoder Select to save the encoder configuration
	         * @param control_limits Select to save the control limit configuration
	         * @param motor_limits Select to save the motor limit configuration
	         * @param motor_params Select to save the motor parameter configuration
	         * @param current_gains Select to save the current gains configuration
	         * @param speed_gains Select to save the speed gains configuration
	         * @param position_gains Select to save the position gains configuration
	         * @param control_options Select to reset the control options configuration
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS saveSelectedConfig(bool encoder, bool control_limits, bool motor_limits, bool motor_params, bool current_gains, bool speed_gains, bool position_gains, bool control_options);

			/** 
	         * \brief Reset the current configuration parameters to factory default.
	         *
	         * Command the module to reset the current configuration parameters on the onboard flash.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS resetConfig();

			/** 
	         * \brief Reset the selected configuration parameters to factory default.
	         *
	         * Command the module to reset the selected configuration parameters on the onboard flash.
	         * @param encoder Select to reset the encoder configuration
	         * @param control_limits Select to reset the control limit configuration
	         * @param motor_limits Select to reset the motor limit configuration
	         * @param motor_params Select to reset the motor parameter configuration
	         * @param current_gains Select to reset the current gains configuration
	         * @param speed_gains Select to reset the speed gains configuration
	         * @param position_gains Select to reset the position gains configuration
	         * @param control_options Select to reset the control options configuration
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS resetSelectedConfig(bool encoder, bool control_limits, bool motor_limits, bool motor_params, bool current_gains, bool speed_gains, bool position_gains, bool control_options);

			/** 
	         * \brief Restore the current configuration parameters to saved values.
	         *
	         * Command the module to restore the current configuration parameters with values stored on the onboard flash.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS restoreConfig();

			/** 
	         * \brief Restore the selected configuration parameters to saved values.
	         *
	         * Command the module to restore the selected configuration parameters with values stored on the onboard flashh.
	         * @param encoder Select to restore the encoder configuration
	         * @param control_limits Select to restore the control limit configuration
	         * @param motor_limits Select to restore the motor limit configuration
	         * @param motor_params Select to restore the motor parameter configuration
	         * @param current_gains Select to restore the current gains configuration
	         * @param speed_gains Select to restore the speed gains configuration
	         * @param position_gains Select to restore the position gains configuration
	         * @param control_options Select to reset the control options configuration
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS restoreSelectedConfig(bool encoder, bool control_limits, bool motor_limits, bool motor_params, bool current_gains, bool speed_gains, bool position_gains, bool control_options);

			/** 
	         * \brief Perform calibration on selected tasks.
	         *
	         * Command the module to commence calibration of selected tasks. During the calibration procedure the motor will perform small movements.
	         *
	         * The module need to be be in Config state for calibration to commence.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         * @see setCalibrationTasks
	         */
			MUSE_COMMAND_STATUS calibrateStart();

			/** 
	         * \brief Set the desired calibration tasks.
	         *
	         * Set the calibration tasks to be performed during the calibration procedure.
	         * @param adc_elec_bias Enable calibration of the Electrical Bias of the ADC
	         * @param elec_angle_offset Enable calibration of the electrical angle offset of teh encoder
	         * @param static_friction Enable static friction calibration
	         * @param viscous_friction Enable viscous friction calibration
	         * @param mech_inertia Enable  mechanical inertia calibration
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS setCalibrationTasks(bool adc_elec_bias, bool elec_angle_offset, bool static_friction, bool viscous_friction, bool mech_inertia);

			/** 
	         * \brief Enable ADC Electrical Bias calibration.
	         *
	         * Set the ADC Electrical Bias calibration task to performed in the calibration procedure.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS calibrateAdcElecBias();

			/** 
	         * \brief Enable Encoder Electrical Angle Offset calibration.
	         *
	         * Set the Encoder Electrical Angle Offset calibration task to performed in the calibration procedure.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS calibrateElecAngleOffset();

			/** 
	         * \brief Enable Static Friction calibration.
	         *
	         * Set the Static Friction calibration task to performed in the calibration procedure.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS calibrateStaticFriction();

			/** 
	         * \brief Enable Viscous Friction calibration.
	         *
	         * Set the Viscous Friction calibration task to performed in the calibration procedure.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS calibrateViscousFriction();

			/** 
	         * \brief Enable Mechanical Inertia calibration.
	         *
	         * Set the Mechanical Inertia calibration task to performed in the calibration procedure.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS calibrateMechInertia();

			/** 
	         * \brief Enable all available calibration tasks.
	         *
	         * Set all available calibration task to performed in the calibration procedure.
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS calibrateAll();

			/** 
	         * \brief Set the desired IP, MAC and Subnet mask to the module.
	         *
	         * Set a static IP address for the module to use and assign a custom MAC address to the Ethernet PHY.
	         * @param IP The byte array that corresponds to the IP address 
	         * @param MAC The byte array that corresponds to the MAC address
	         * @param SUBNETMASK The number of 1s in the subnet mask
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS SetIPandMAC(uint8_t IP[4], uint8_t MAC[6], uint8_t SUBNETMASK);

			/** 
	         * \brief Set the desired Name to the module.
	         *
	         * Set a Name that will act as an identifier for the module when it appears online.
	         * @param name The string representing the desired name. Up to 22 charachters length
	         * @return The MUSE_COMMAND_STATUS enum
	         * @see MUSE_COMMAND_STATUS
	         */
			MUSE_COMMAND_STATUS SetName(std::string name);

		private :

			void parseMessage();

			void formMessage();

			bool sendCommand;

			bool gotACK;

			bool gotConfig;

			bool requestConfig;

			bool b_enabled;

			bool b_error;

			bool b_fault;

			bool b_configured;

			bool b_calibrated;

			int printcount;

			rtmcp_pmsm_t o_pmsm_info;

			muse_brushless_fsm_state o_fsm_state;

			muse_brushless_control_mode o_control_mode;

			muse_brushless_error e_brushless_error;

			muse_brushless_fault e_brushless_fault;

			muse_message_brushless o_brushless_message_in;

			muse_message_brushless o_brushless_message_null;

			muse_message_generic msg_helper;
	};

	/**
	 * \class muse_module_finder
	 * \brief The module fidner class.
	 *
	 * The muse_module_finder class listen for broadcast messages from modules on the network and maintains a list of online modules and their status
	 */
	class muse_module_finder
	{
		private :
			bool bSocketOpen;
			std::map<std::string, muse_module_info> mModuleMap;
			pthread_t finderThread;
			pthread_mutex_t listMutex;
			pthread_mutex_t stopMutex;
			std::map<std::string, muse_module_info> retMap;
			void * finderFunc();
			static void * finderFuncHelper(void *context) { return ((muse_module_finder *)context)->finderFunc(); };
			int AddModule(std::string _sIP, std::string _sName, char* _mac, uint8_t netmaskones);
			int RemoveModule(std::string _sIP);
			void moduleFinderPoll();
			void CheckTimeout(void);

		public :

			/** 
	         * \brief The base constructor
	         *
	         * Creates an muse_module_finder object.
	         */
			muse_module_finder();

			/** 
	         * \brief The base destructor
	         *
	         * Destroys an muse_module_finder object terminating the listening thread.
	         */
			~muse_module_finder();

			/** 
	         * \brief Start listening for broadcast messages from online modules
	         *
	         * Starts the thread which opens a udp socket and listens for broadcast messages from online modules.
	         */
			void StartListening(void);

			/** 
	         * \brief Stop listening for broadcast messages from online modules
	         *
	         * Stops the thread and closes the udp socket that listens for broadcast messages from online modules.
	         */
			void StopListening(void);

			/** 
	         * \brief Get the list of online modules
	         *
	         * Retrive the std::map of online modules. Key is the module IP and value is the muse_module_info struct.
	         * @see muse_module_info
	         */
			std::map<std::string, muse_module_info> &GetModuleList(void);

			/** 
	         * \brief Update the information of a module on the list
	         *
	         * Update the muse_module_info struct for an online module.
	         * @param _sIP The desired module IP in string format
	         * @param _cMod The muse_module_info struct that provides the update information
	         * @see muse_module_info
	         */
			int UpdateModule(std::string _sIP, muse_module_info _cMod);

			/** 
	         * \brief Print the list of online modules along with their information
	         *
	         * Provide a list of online modules and their respective information on the standard output.
	         */
			void DisplayActiveModules(void);

			/** 
	         * \brief Get the status of the listening socket
	         *
	         * Lets us know if the socket that listens for broadcast messages from online modules is open or not.
	         * @return True or False if the socket is open or not
	         */
			bool IsSocketOpen(void);

	};
}

#endif