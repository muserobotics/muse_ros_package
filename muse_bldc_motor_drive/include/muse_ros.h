#include "ros/ros.h"
#include "muse.h"
#include "muse_bldc_motor_drive/state_machine_cmd.h"
#include "muse_bldc_motor_drive/control_cmd.h"
#include "muse_bldc_motor_drive/feedback.h"
#include "muse_bldc_motor_drive/communication_cmd.h"


using namespace std;
using namespace Muse;

enum COMMANDS
{
	NONE = 0,
	CONNECT,					// STATE MACHINE TASKS
	DISCONNECT,
	STARTUP,
	SHUTDOWN,
	ENABLE_SERVO,
	DISABLE_SERVO,
	CONTROL_ON,
	CONTROL_OFF,
	QUICK_ENABLE,
	CALIBRATE_ELEC_ANGLE_OFFSET,
	CALIBRATE_START,
	SET_NAME,				// set name
	CLEAR_FAULTS,			// clear errors/faults 
	CLEAR_ERRORS,
	SET_CONTROL_LIMITS,		// set control configs
	SET_CURRENT_GAINS,
	SET_SPEED_GAINS,
	SET_POSITION_GAINS,
	SET_CURRENT,			// control 
	SET_SPEED, 
	SET_POSITION
};

/**
 *	\class ModuleTopicsHandler 
 *	\brief This class handles appropriate topics for ROS communication with a single Muse Module. 
 *	
 *	Creates feedback publisher and subscribes to two topics: state machine commands topic & control commands topic.
 *	  
 */
class ModuleTopicsHandler
{
public:
	ros::Publisher fb_pub;

	ModuleTopicsHandler(ros::NodeHandle* nh, string fb_topic, string state_machine_cmd_topic, string control_cmd_topic, string id);
	~ModuleTopicsHandler();

	/**
	* 	Constructs feedback message for Muse Module 
	*/
	void constructFeedbackMsg(muse_bldc_motor_drive::feedback& msg);

	/**
	*	Callback function that subscribes to state machine commands topic
	*/
	void modStateMachineCmdCallback(const muse_bldc_motor_drive::state_machine_cmd& msg);
	
	/**
	* 	Callback function that subscribes to control commands topic
	*/
	void modControlCommandsCallback(const muse_bldc_motor_drive::control_cmd& msg);
	
	/**
	*	Returns object's id => hence Muse Module's IP  
	*/
	string getId();
	
	/**
	*	Disconnects module and destroys publisher and subscriber   
	*/
	void shutdown();

	/**
	 * 	Returns true if erase command has been sent, otherwise false.
	 *
	 */
	bool getEraseCommand();

	/**
	 * 	Returns true if erase command has been sent, otherwise false.
	 *
	 */
	void setEraseCommand(bool cmd);


private:
	bool _eraseFromVector;
	string _id; 		//	module ip
	ros::NodeHandle* _nh;
	ros::Subscriber _state_machine_sub;
	ros::Subscriber _control_sub;

	muse_module_brushless* _my_module;
	muse_module_info _my_module_info;

	// error/faults maps
	map<uint32_t, string> _errors;
	map<uint32_t, string> _faults;

	void fillErrorMaps();
};

/**
 *	\class MuseRosHandler 
 *	\brief This class creates muse/communication subscriber and sets control frequence.
 *	
 */
class MuseRosHandler
{
public:
	MuseRosHandler(ros::NodeHandle* nh, double spin_freq);

	/**
	* 	Callback function that subscribes to communication commands topic
	*/
	void communicationCallback(const muse_bldc_motor_drive::communication_cmd& msg);

private:
	double _SPIN_FREQ;
	ros::NodeHandle* _nh;
	ros::Subscriber _muse_communication_sub; 
	bool _running_node;

	void* spinCallbacksThread();
	static void * spinThread_helper(void *context) { return ((MuseRosHandler *)context)->spinCallbacksThread(); };
};