/***************************************************************************************
*
*  Authors:        Maria Karageorgiou
*
*  File:           current_demo.cpp
*
*  Description:    MUSE Test application with sine command in current control mode.
*
*
*  Copyright (C) 2015 Muse Robotics Incorporated - http://www.muserobotics.com
*
***************************************************************************************/

#include "ros/ros.h"
#include "muse.h"
#include "muse_ros.h"
#include "muse_bldc_motor_drive/communication_cmd.h"
#include "muse_bldc_motor_drive/control_cmd.h"
#include "muse_bldc_motor_drive/state_machine_cmd.h"
#include "muse_bldc_motor_drive/feedback.h"

#include <iostream>
#include <fstream>

#define DEMO_CONTROL_FREQ_HZ	500

using namespace std;

bool module_connected;
muse_brushless_fsm_state module_fsm_state;
double fb_point = 0;

// log files
ofstream spFile;
ofstream fbFile;
struct timeval tp;

void museFeedbackCallback(const muse_bldc_motor_drive::feedback& msg);

int main(int argc, char **argv)
{
	// define messages 
	muse_bldc_motor_drive::communication_cmd comm_msg;
	muse_bldc_motor_drive::state_machine_cmd state_machine_msg;
	muse_bldc_motor_drive::control_cmd control_msg;

	double time_counter = 0;
    double timestep = (1.0/DEMO_CONTROL_FREQ_HZ);
	double duration = 60; 	// seconds
	                     	
	ros::init(argc, argv, "current_demo");
	ros::NodeHandle nh;
	/*
	* 	publish to "muse_bldc_motor_drive/communication" topic, to tell muse node to open the appropriate topics for communication with the drive 
	*/
	ros::Publisher muse_communication_pub = nh.advertise<muse_bldc_motor_drive::communication_cmd>("muse_bldc_motor_drive/communication", 10);
	/*
	* 	this is the desired topic for state machine commands 
	*/
	ros::Publisher fha_smc_pub = nh.advertise<muse_bldc_motor_drive::state_machine_cmd>("fha_smc_topic", 10);
	// ros::Publisher devel_smc_pub = nh.advertise<muse_bldc_motor_drive::state_machine_command>("devel_smc_topic", 10);
	/*
	* 	this is the desired topic for control commands
	*/
	ros::Publisher fha_ctrl_pub = nh.advertise<muse_bldc_motor_drive::control_cmd>("fha_ctrl_topic", 10);
	// ros::Publisher devel_ctrl_pub = nh.advertise<muse_bldc_motor_drive::control_command>("devel_ctrl_topic", 10);
	/*
	* 	this is the desired topic for getting feedback 
	*/
	ros::Subscriber fha_fb_sub = nh.subscribe("fha_fb_topic", 1, museFeedbackCallback);
	// ros::Subscriber devel_fb_sub = nh.subscribe("devel_fb_topic", 1, museFeedbackCallback);

	/*
	*	demo frequency
	*/
  	ros::Rate loop_rate(DEMO_CONTROL_FREQ_HZ);
  	/*
  	*	sleep for a few seconds, until ros master opens topics above.
  	*/
  	ros::Duration(2).sleep();
	
	/*
	* 	construct the appropriate message to tell muse node
	* 	to open topics for communication (state machine commands topic, control topic, feedback topic)
	*/
	comm_msg.start_communication = true;
	comm_msg.ip_or_name = "192.168.1.48";
	comm_msg.state_machine_commands_topic = "fha_smc_topic";
	comm_msg.control_commands_topic = "fha_ctrl_topic";
	comm_msg.feedback_topic = "fha_fb_topic";
	muse_communication_pub.publish(comm_msg);
	
	ros::Duration(1).sleep();

  	cout << "Connecting to " << comm_msg.ip_or_name << endl;
	
	state_machine_msg.id_cmd = CONNECT;
	fha_smc_pub.publish( state_machine_msg);

	ros::Duration(0.5).sleep();
 // 	state_machine_msg.id_cmd = QUICK_ENABLE;
	// fha_smc_pub.publish( state_machine_msg);

	// ros::Duration(2.0).sleep();

	while(ros::ok() && time_counter <= duration)
	{
		// construct message for motor enable
		// state_machine_msg.id_cmd = CONNECT;
		// state_machine_msg.sarg = "";
		// fha_smc_pub.publish( state_machine_msg);

		cout << "feedback current: " << fb_point << endl;

		ros::Duration(2.0).sleep();

		// cout << "disconnecting... " << time_counter << endl;

        time_counter += timestep;	 		
  		ros::spinOnce();
  		loop_rate.sleep();
	}

	cout << "exiting..." << endl;

 	state_machine_msg.id_cmd = DISCONNECT;
	fha_smc_pub.publish( state_machine_msg);
	ros::Duration(1.0).sleep();

	comm_msg.start_communication = false;
	comm_msg.ip_or_name = "MUSE-DEVEL-1";
	muse_communication_pub.publish(comm_msg);
	ros::Duration(2.0).sleep();	


	// ros::Duration(15).sleep();
}


void museFeedbackCallback(const muse_bldc_motor_drive::feedback& msg)
{
	if (msg.fsm_state != 0) module_connected = true;
	else module_connected = false;

	module_fsm_state = muse_brushless_fsm_state(msg.fsm_state);

    // store
    fb_point = msg.current;
}