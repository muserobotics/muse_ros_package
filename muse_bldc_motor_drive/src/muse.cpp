#include <map>
#include "muse_ros.h"
#include "muse_bldc_motor_drive/module_id.h"
#include "muse_bldc_motor_drive/module_list.h"

using namespace std;
using namespace Muse;

/*
* 	necessary variables for creating new topics
*/
bool start_communication_topics;
bool stop_communication_topics;
string control_topic;
string state_machine_topic;
string feedback_topic;


/*
*	 communication_id = module ip for start/stop communication
*/
string communication_id;	

/*
*	vector filled with ModuleTopicsHandle instances
*	one instance for each Muse Module ROS communication
*/
vector<ModuleTopicsHandler*> modTopicsHandlerArray;
string eraseModTopicsHandlerFromVector;

/*
*	vector filled with communication id's that are currently communicating with Muse Node
*	useful for quick searching
*/
vector<string> communication_modules;
/*
* 	module finder instance 
* 	and dictionary with active Muse Modules 
*/
muse_module_finder _module_finder;
map<string, muse_module_info> _local_module_map;

void constructModuleFinderMsg(muse_bldc_motor_drive::module_list& list_msg);
void fillErrorMaps();

void quit(int sig)
{
	if(_module_finder.IsSocketOpen())
	    _module_finder.StopListening();

	for (std::vector<ModuleTopicsHandler*>::iterator itModTopHandler = modTopicsHandlerArray.begin(); itModTopHandler != modTopicsHandlerArray.end(); ++itModTopHandler)
	{
		(*itModTopHandler)->shutdown();
		modTopicsHandlerArray.erase(itModTopHandler);
	}

	ROS_INFO("Exiting...");
	ros::Duration(0.01).sleep();
	exit(0);
}

int main(int argc, char **argv)
{
	double MUSE_SPIN_FREQ;
    double MUSE_FEEDBACK_FREQ_HZ;
    if (argc != 3)
    {
        cerr << "Usage: " << argv[0] << " <feedback_frequency(Hz)> <control_frequency(Hz)" << endl;
        MUSE_SPIN_FREQ = 1000.0;
        MUSE_FEEDBACK_FREQ_HZ = 1000.0;
        cout << "Using default frequency for both: 1 KHz." << endl;
        // return -1;
    }
    else
    {
        istringstream iss1( argv[1]);
        istringstream iss2( argv[2]);

        if (!(iss1 >> MUSE_SPIN_FREQ)) 
            cerr << "Invalid number " << argv[1] << '\n';

        if (!(iss2 >> MUSE_FEEDBACK_FREQ_HZ)) 
            cerr << "Invalid number " << argv[2] << '\n';

        ROS_INFO("Muse Node Frequency for control commands: %f. Feedback frequency %f Hz.)", MUSE_SPIN_FREQ, MUSE_FEEDBACK_FREQ_HZ);
    }

  	signal(SIGINT,quit);

	muse_bldc_motor_drive::module_list mod_list_msg;
	muse_bldc_motor_drive::feedback fb_msg;
	
	ros::init(argc, argv, "muse_bldc_motor_drive");
	ros::NodeHandle museNodeHandler;
	// Initialize the publisher for board discovery post
	ros::Publisher muse_mod_list_pub = museNodeHandler.advertise<muse_bldc_motor_drive::module_list>("muse_bldc_motor_drive/module_discovery", 1);
  	ros::Rate loop_rate_fb(MUSE_FEEDBACK_FREQ_HZ);

  	if (!_module_finder.IsSocketOpen())
  		_module_finder.StartListening();

	MuseRosHandler museRosHandler(&museNodeHandler, MUSE_SPIN_FREQ);

  	uint counter = 0;
  	while (ros::ok())
  	{
  		/*
		* construct and publish module finder list  		
		*/
  		if (_module_finder.IsSocketOpen() && counter >= 10)
  		{
  			_local_module_map = _module_finder.GetModuleList();
  			constructModuleFinderMsg(mod_list_msg);
  			muse_mod_list_pub.publish(mod_list_msg);
  			counter = 0;
  		}

		/*
		*	publish feedback
		*/
		for (std::vector<ModuleTopicsHandler*>::iterator itModTopHandler = modTopicsHandlerArray.begin(); itModTopHandler != modTopicsHandlerArray.end(); ++itModTopHandler)
		{
			if (eraseModTopicsHandlerFromVector != (*itModTopHandler)->getId())
			{
				(*itModTopHandler)->constructFeedbackMsg( fb_msg);
				if ((*itModTopHandler)->fb_pub != NULL)
					(*itModTopHandler)->fb_pub.publish(fb_msg);
				else
					ROS_INFO("no publisher.");
			}
			else
				(*itModTopHandler)->setEraseCommand(true);
		}

  		counter++;
  		loop_rate_fb.sleep();
  	}

  	if (_module_finder.IsSocketOpen())
  		_module_finder.StopListening();

	for (std::vector<ModuleTopicsHandler*>::iterator itModTopHandler = modTopicsHandlerArray.begin() ; itModTopHandler != modTopicsHandlerArray.end(); ++itModTopHandler)
	{
		(*itModTopHandler)->shutdown();
	}

  	return 0;
}

void constructModuleFinderMsg(muse_bldc_motor_drive::module_list& list_msg)
{
	list_msg.lModulesList.clear();
	map<string, muse_module_info>::iterator iModIterator;
	muse_module_info tmp_mod_info;
	muse_bldc_motor_drive::module_id tmp_mod_id;
	int mod_index = 0;

	list_msg.uActiveModules = 0;

	if(!_local_module_map.empty())
	{
		for(iModIterator = _local_module_map.begin(); iModIterator != _local_module_map.end(); ++iModIterator)
		{
			tmp_mod_info = iModIterator->second;
			tmp_mod_id.mod_name =  tmp_mod_info.module_name ;
			tmp_mod_id.mod_ip = tmp_mod_info.module_ip;
			switch (tmp_mod_info.module_status)
			{
				case MUSE_STATUS_EMPTY:
					tmp_mod_id.mod_status = "empty";
					break;
				case MUSE_STATUS_IDLE:
					tmp_mod_id.mod_status = "idle";
					break;
				case MUSE_STATUS_CONNECTED:
					tmp_mod_id.mod_status = "connected";
					break;
				case MUSE_STATUS_DISCONNECTED:
					tmp_mod_id.mod_status = "disconnected";
					break;				
				case MUSE_STATUS_ERROR:
					tmp_mod_id.mod_status = "error";
					break;
				case MUSE_STATUS_TIMEOUT:
					tmp_mod_id.mod_status = "timeout";
					break;
			}
			list_msg.lModulesList.push_back( tmp_mod_id);
			list_msg.uActiveModules++;
			// cout << tmp_mod_info.module_name << " " << tmp_mod_info.module_ip << " status: " << to_string(tmp_mod_info.module_status, 1) << endl;
		}
	}
}

ModuleTopicsHandler::ModuleTopicsHandler(ros::NodeHandle* nh, string fb_topic, string state_machine_cmd_topic, string control_cmd_topic, string id)
{
	_my_module_info.module_ip = id;
	_my_module = new muse_module_brushless(_my_module_info);
	_nh = nh;
	fillErrorMaps();
	          		 
	fb_pub = _nh->advertise<muse_bldc_motor_drive::feedback>(fb_topic, 1);
	_state_machine_sub =  _nh->subscribe(state_machine_cmd_topic, 10, &ModuleTopicsHandler::modStateMachineCmdCallback, this);
	_control_sub = _nh->subscribe(control_cmd_topic, 10, &ModuleTopicsHandler::modControlCommandsCallback, this);
	_id = id;
}

ModuleTopicsHandler::~ModuleTopicsHandler()
{
	if (_my_module->IsConnected())
		_my_module->Disconnect();

	ros::Duration(0.01).sleep();
}

void ModuleTopicsHandler::fillErrorMaps()
{
	_errors.insert( pair<uint32_t, string>(0, "None"));
	_errors.insert( pair<uint32_t, string>(1, "Undervoltage"));
	_errors.insert( pair<uint32_t, string>(2, "Overvoltage"));
	_errors.insert( pair<uint32_t, string>(4, "High Temperature"));
	_errors.insert( pair<uint32_t, string>(8, "Non-Calibrated Encoder"));
	_errors.insert( pair<uint32_t, string>(16, "Configuration"));

	_faults.insert( pair<uint32_t, string>(0, "None"));
	_faults.insert( pair<uint32_t, string>(1, "Power Stage"));
	_faults.insert( pair<uint32_t, string>(2, "Internal Gate Driver Error"));
	_faults.insert( pair<uint32_t, string>(4, "Overcurrent"));
	_faults.insert( pair<uint32_t, string>(8, "Overvoltage"));
	_faults.insert( pair<uint32_t, string>(16, "Temperature Too High"));
	_faults.insert( pair<uint32_t, string>(32, "Undervoltage"));
	_faults.insert( pair<uint32_t, string>(64, "Encoder Malfunction or Non-Calibrated Encoder"));
	_faults.insert( pair<uint32_t, string>(128, "Configuration"));
}

void ModuleTopicsHandler::constructFeedbackMsg(muse_bldc_motor_drive::feedback& msg)
{
	if (_my_module->IsConnected())
	{
		msg.module_name = _my_module_info.module_name;
		msg.current = _my_module->getCurrent();
		msg.speed = _my_module->getSpeed();
		msg.position = _my_module->getPosition();
		msg.bus_voltage = _my_module->getBusVoltage();
		msg.temperature = _my_module->getMcuTemperature();
        msg.control_mode = _my_module->getControlMode();
        msg.fsm_state = _my_module->getFsmState();
        
        uint32_t error_value = _my_module->getError().value;
        uint32_t total_errors = 0;
        uint32_t mask = 0x01;
        for (int i=0; i<_errors.size(); i++)
        {
        	total_errors += error_value & mask;
        	// cout << "error " << msg.error << endl;
        	mask = mask << 1;
        }
    	msg.error = _errors[ total_errors];

        uint32_t fault_value = _my_module->getFault().value;
        uint32_t total_faults = 0;
        mask = 0x01;
        for (int i=0; i<_faults.size(); i++)
        {
        	total_faults += fault_value & mask;
        	// cout << "fault " << msg.fault << endl;
        	mask = mask << 1;
        }
    	msg.fault = _faults[ total_faults];
	}
	else
	{
		msg.module_name = "none";
		msg.current = 0;
		msg.speed = 0;
		msg.position = 0;
		msg.bus_voltage = 0;
		msg.temperature = 0;
    	msg.control_mode = 0;
    	msg.fsm_state = 0;
    	msg.error = "-";
    	msg.fault = "-";
	}
}

void ModuleTopicsHandler::modStateMachineCmdCallback(const muse_bldc_motor_drive::state_machine_cmd& msg)
{
	if (msg.id_cmd == CONNECT)
	{
		map<string, muse_module_info>::iterator iModIterator;	
		for(iModIterator = _local_module_map.begin(); iModIterator != _local_module_map.end(); ++iModIterator)
		{
			muse_module_info tmp_mod_info = iModIterator->second;
			if (tmp_mod_info.module_ip == _my_module_info.module_ip && tmp_mod_info.module_status == MUSE_STATUS_CONNECTED)
			{
				ROS_INFO("Module %s is already connected. State machine command ignored.", tmp_mod_info.module_name.c_str());
				// cout << "Module " << tmp_mod_info.module_name << " is already connected. State machine command ignored." << endl;
				return;
			}
		}

		/*
		 * connect to drive
		 */
		// check ip address
		in_addr_t net_addr = inet_network(_my_module_info.module_ip.c_str());
		if (net_addr != -1)
		{
			// find module ip in local map	and connect
			map<string, muse_module_info>::iterator iModIterator;
			if(!_local_module_map.empty())
			{
				for(iModIterator = _local_module_map.begin(); iModIterator != _local_module_map.end(); ++iModIterator)
				{
					// cout << (iModIterator->second).module_ip << " " << msg.ip_or_name << endl; 
					if ((iModIterator->second).module_ip == _my_module_info.module_ip)
					{
						_my_module_info.module_ip = (iModIterator->second).module_ip;
						_my_module_info.module_name = (iModIterator->second).module_name;
						_my_module_info.module_status = (iModIterator->second).module_status;
						_my_module = new muse_module_brushless(_my_module_info);
						_my_module->Connect();
						// cout << "Connecting to " << _my_module_info.module_ip << _my_module_info.module_name << endl;
						break;
					}
				}
			}
		}
		else 
			ROS_INFO("Invalid ip.");	
	}
	else if (_my_module->IsConnected())
	{
		switch (msg.id_cmd)
		{
			case DISCONNECT:
				_my_module->Disconnect();
				break;
			case STARTUP:
				_my_module->StartUp();
				break;
			case SHUTDOWN:
				_my_module->ShutDown();
				break;
			case ENABLE_SERVO:
				_my_module->enableServo();
				break;
			case DISABLE_SERVO:
				_my_module->disableServo();
				break;
			case CONTROL_ON:
				_my_module->controlOn();
				break;
			case CONTROL_OFF:
				_my_module->controlOff();
				break;
			case QUICK_ENABLE:
				_my_module->quickEnable();
				break;
			case CALIBRATE_ELEC_ANGLE_OFFSET:
				_my_module->calibrateElecAngleOffset();
				break;
			case CALIBRATE_START:
				_my_module->calibrateStart();
				break;
			case SET_NAME:
				_my_module->SetName(msg.sarg);
				break;
			case CLEAR_ERRORS:
				_my_module->clearErrors();
				break;
			case CLEAR_FAULTS:
				_my_module->clearFaults();
				break;
			default:
				ROS_INFO("Invalid command id"); 
				break;
		}
	}
	else
		ROS_INFO("Drive is not connected.");
}

void ModuleTopicsHandler::modControlCommandsCallback(const muse_bldc_motor_drive::control_cmd& msg)
{
	if (_my_module->IsConnected())
	{
		switch (msg.id_cmd)
		{
			case SET_CURRENT:
				_my_module->setCurrent(msg.darg[0]);
				break;
			case SET_SPEED:
				_my_module->setSpeed(msg.darg[0]);
				break;
			case SET_POSITION:
				_my_module->setPosition(msg.darg[0]);
				break;
			case SET_CONTROL_LIMITS:
				_my_module->setLimits(msg.darg[0], msg.darg[1], msg.darg[2], msg.darg[3], msg.darg[4]);
				break;
			case SET_CURRENT_GAINS:
				_my_module->setCurrentGains(msg.darg[0], msg.darg[1]);
				break;
			case SET_SPEED_GAINS:
				_my_module->setSpeedGains(msg.darg[0], msg.darg[1]);
				break;
			case SET_POSITION_GAINS:
				_my_module->setPositionGains(msg.darg[0], msg.darg[1]);
				break;
			default:
				_my_module->setCurrent(0); 
				break;
		}
	}
}

string ModuleTopicsHandler::getId()
{
	return _id;
}

void ModuleTopicsHandler::shutdown()
{
	// cout << "deleting topics..." << endl;
	if (_my_module->IsConnected())
		_my_module->Disconnect();
  	
  	// ros::Duration(0.001).sleep();

	_state_machine_sub.shutdown();
	_control_sub.shutdown();
	fb_pub.shutdown();
}


bool ModuleTopicsHandler::getEraseCommand()
{
	return _eraseFromVector;
}

void ModuleTopicsHandler::setEraseCommand(bool cmd)
{
	_eraseFromVector = cmd;
}

// struct timeval tp;
// long int init_time_ms;
// long int time_ms;

MuseRosHandler::MuseRosHandler(ros::NodeHandle* nh, double spin_freq)
{
 //    gettimeofday(&tp, NULL);
	// init_time_ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;

	_SPIN_FREQ = spin_freq;
	_running_node = true;
	_nh = nh;

	// Initialize subscriber for module status updates
	_muse_communication_sub = _nh->subscribe("muse_bldc_motor_drive/communication", 10, &MuseRosHandler::communicationCallback, this);

    pthread_t spinThread;
    int iret = pthread_create( &spinThread, NULL, &MuseRosHandler::spinThread_helper, this);
    if(iret)
		cout <<"Error - pthread_create() return code: " << iret << endl;
    else
        pthread_detach(spinThread);
}

void MuseRosHandler::communicationCallback(const muse_bldc_motor_drive::communication_cmd& msg)
{
	if (msg.start_communication)
	{
		ros::master::V_TopicInfo master_topics;
		ros::master::getTopics(master_topics);

		// bool asked_for_new_topics = true;
		/*
		*	Check if requested topics' names are already in ros master
		*/
		for (ros::master::V_TopicInfo::iterator it = master_topics.begin() ; it != master_topics.end(); it++) 
		{
			const ros::master::TopicInfo& info = *it;
			// std::cout << "topic_" << it - master_topics.begin() << ": " << info.name << std::endl;
			if (info.name == msg.state_machine_commands_topic || info.name == msg.control_commands_topic || info.name == msg.feedback_topic)
			{
				// asked_for_new_topics = false;
				ROS_INFO("There is already a topic with the same name.");
				return;
			}
		}	

		/*
		 *	Check if requested drive is visible by module finder
		 *	and hold communication_id (ip)
		 */
		bool module_is_visible = false;
		in_addr_t net_addr = inet_network(msg.ip_or_name.c_str());
		if (net_addr != -1)
		{
			// find module IP in local map 
			map<string, muse_module_info>::iterator iModIterator;
			if(!_local_module_map.empty())
			{
				for(iModIterator = _local_module_map.begin(); iModIterator != _local_module_map.end(); ++iModIterator)
				{
					if ((iModIterator->second).module_ip == msg.ip_or_name)
					{
						module_is_visible = true;
						communication_id = msg.ip_or_name;
						break;
					}
				}
			}
		}
		else 
		{
			// find module NAME in local map and connect
			map<string, muse_module_info>::iterator iModIterator;
			if(!_local_module_map.empty())
			{
				for(iModIterator = _local_module_map.begin(); iModIterator != _local_module_map.end(); ++iModIterator)
				{
					if ((iModIterator->second).module_name == msg.ip_or_name)
					{
						module_is_visible = true;
						communication_id = (iModIterator->second).module_ip;
						break;
					}
					// else
						// ROS_INFO("Module not found by name.");
				}
			}					
		}

		if (!module_is_visible)
		{
			ROS_INFO("Module not found.");
			return;
		}

		/*
		* 	Check if there is an instance running for this drive and do not start a new one
		*/
		for (std::vector<ModuleTopicsHandler*>::iterator itModTopHandler = modTopicsHandlerArray.begin(); itModTopHandler != modTopicsHandlerArray.end(); ++itModTopHandler)
		{
			if ((*itModTopHandler)->getId() == communication_id)
			{
				// (*itModTopHandler)->shutdown();
				// modTopicsHandlerArray.erase(itModTopHandler);
				// ROS_INFO("Drive %s is already running.", communication_id.c_str());
				return;
			}
		}

		start_communication_topics = true; 
		control_topic = msg.control_commands_topic;
		state_machine_topic = msg.state_machine_commands_topic;
		feedback_topic = msg.feedback_topic;
		communication_id = msg.ip_or_name;
	}
	else
	{
		string mod_id = msg.ip_or_name;
		bool id_is_name = true;
		in_addr_t net_addr = inet_network(msg.ip_or_name.c_str());
		if (net_addr != -1)
			id_is_name = false;

		// find module's ip
		if (id_is_name)
		{
			// find module NAME in local map and connect
			map<string, muse_module_info>::iterator iModIterator;
			if(!_local_module_map.empty())
			{
				for(iModIterator = _local_module_map.begin(); iModIterator != _local_module_map.end(); ++iModIterator)
				{
					if ((iModIterator->second).module_name == msg.ip_or_name)
					{
						mod_id = (iModIterator->second).module_ip;
						break;
					}
					// else
						// ROS_INFO("Module not found by name.");
				}
			}			
		}

		bool asked_to_delete_open_topic = false;
		for (vector<string>::iterator itMod = communication_modules.begin() ; itMod != communication_modules.end(); itMod++) 
		{
			if (*itMod == mod_id)
			{
				asked_to_delete_open_topic = true;
				break;
			}
		}	

		if (asked_to_delete_open_topic)
		{
			stop_communication_topics = true;
			control_topic = msg.control_commands_topic;
			state_machine_topic = msg.state_machine_commands_topic;
			feedback_topic = msg.feedback_topic;
			communication_id = mod_id;
		}
	}
}

void* MuseRosHandler::spinCallbacksThread()
{
  	ros::Rate loop_rate_fb(_SPIN_FREQ);

	while (ros::ok() && _running_node)
	{
  		/*
  		 * start/stop communication topics 
  		*/
  		if (start_communication_topics)
  		{
  			// modTopicsHandler = new ModuleTopicsHandler(&museNodeHandler, feedback_topic, state_machine_topic, control_topic, communication_id);
  			modTopicsHandlerArray.push_back( new ModuleTopicsHandler(_nh, feedback_topic, state_machine_topic, control_topic, communication_id));
  			// delete modTopicsHandler;
  			communication_modules.push_back( communication_id);
  			start_communication_topics = false;
  		}
		else if (stop_communication_topics)
		{
			eraseModTopicsHandlerFromVector = communication_id;
			stop_communication_topics = false;
		}
		else if (!eraseModTopicsHandlerFromVector.empty())		
		{
			for (std::vector<ModuleTopicsHandler*>::iterator itModTopHandler = modTopicsHandlerArray.begin(); itModTopHandler != modTopicsHandlerArray.end(); ++itModTopHandler)
			{
				if ((*itModTopHandler)->getId() == eraseModTopicsHandlerFromVector && (*itModTopHandler)->getEraseCommand())
				{
					(*itModTopHandler)->shutdown();
				 	modTopicsHandlerArray.erase(itModTopHandler);
					(*itModTopHandler)->setEraseCommand(false);
					eraseModTopicsHandlerFromVector = "";
					break;
				}
			}	
			for (std::vector<string>::iterator itMod = communication_modules.begin(); itMod != communication_modules.end(); ++itMod)
			{
				if (*itMod == communication_id)
				{
					communication_modules.erase(itMod);
					break;
				}
			}		
		}

	
	    // gettimeofday(&tp, NULL);
	    // time_ms = (tp.tv_sec * 1000 + tp.tv_usec / 1000) - init_time_ms; //get current timestamp in milliseconds 
	    // cout << time_ms << endl;

  		ros::spinOnce();
  		loop_rate_fb.sleep();
	}
}
